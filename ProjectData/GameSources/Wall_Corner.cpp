#include "stdafx.h"
#include "Project.h"

namespace basecross {
	void Wall_Corner::OnCreate() {
		SetTrans();

		auto ptrTransform = GetComponent<Transform>();

		//物理計算ボックス
		PsBoxParam param(ptrTransform->GetWorldMatrix(), 0.0f, true, PsMotionType::MotionTypeFixed);
		auto PsPtr = AddComponent<RigidbodyBox>(param);

		//タグをつける
		AddTag(L"Wall");
		SetModelName(L"m_wall_corner.bmf");
		ReadBmfData();

	}
}