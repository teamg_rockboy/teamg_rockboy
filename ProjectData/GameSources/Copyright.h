#pragma once
#include "stdafx.h"

namespace basecross {

	class CopyrightStage : public Stage {
		void CreateCamera();
		void CreateUI();
	public:
		CopyrightStage() :Stage() {}
		virtual ~CopyrightStage() {}

		virtual void OnCreate()override;
		virtual void OnUpdate()override;

	};

}