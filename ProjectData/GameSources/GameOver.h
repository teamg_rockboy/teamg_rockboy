#pragma once
#include "stdafx.h"

namespace basecross {

	class GameOverStage : public Stage {
		void CreateCamera();
		void CreateUI();
		bool m_select = false;
	public:
		GameOverStage() :Stage() {}
		virtual ~GameOverStage() {}

		virtual void OnCreate()override;
		virtual void OnUpdate()override;
	};

}