#pragma once
#include "stdafx.h"

namespace basecross {
	class ObjectBase : public GameObject {
		Vec3 m_Scale;
		Vec3 m_Rotation;
		Vec3 m_Position;
		Vec3 m_ModelScale;
		Vec3 m_ModelPosition;
		Vec3 m_effectScale;

		wstring m_modelName;
		wstring m_effectStr;
		shared_ptr<EfkEffect> m_efkEffect;
		shared_ptr<EfkPlay> m_efkPlay;

		shared_ptr<PNTStaticModelDraw> m_ptrDraw;
		Col4 m_objectColor = Col4(0.6f, 0.6f, 0.6f, 1.0f);
		float m_flashingTime = 0;

		Mat4x4 m_spanMat; // モデルとトランスフォームの間の差分行列

		int m_effectNum;

	public:
		ObjectBase(const shared_ptr<Stage>& StagePtr,
			const Vec3& Scale,
			const Vec3& Rotation,
			const Vec3& Position,
			const Vec3& ModelScale,
			const Vec3& ModelPosition
			);

		virtual ~ObjectBase() {};

		//--------------------------------------------------------------------------------------
		///	m_modelNameのセッター
		//--------------------------------------------------------------------------------------
		void SetModelName(wstring fileName);

		//--------------------------------------------------------------------------------------
		///	m_effectStrのセッター
		//--------------------------------------------------------------------------------------
		void SetEffectNum(int efk);

		Mat4x4 GetSpanMat() {
			return m_spanMat;
		}

		//--------------------------------------------------------------------------------------
		///	BMFデータ（3Dモデル）を設定
		//--------------------------------------------------------------------------------------
		virtual void ReadBmfData();

		//--------------------------------------------------------------------------------------
		///	トランスフォームの設定
		//--------------------------------------------------------------------------------------
		virtual void SetTrans();

		//--------------------------------------------------------------------------------------
		///	エフェクトの大きさ設定
		//--------------------------------------------------------------------------------------
		void SetEffectScale(Vec3 scale);

		//--------------------------------------------------------------------------------------
		///	オブジェクトの色設定
		//--------------------------------------------------------------------------------------
		void SetObjectColor(Col4 col);

		//--------------------------------------------------------------------------------------
		/// 宣言したオブジェクトの破棄
		//--------------------------------------------------------------------------------------
		void MyDesObject();

		//--------------------------------------------------------------------------------------
		/// エフェクトの再生
		//--------------------------------------------------------------------------------------
		void PlayEffect();

		void VisibleBuilding();

		void FlashingObject();

	};

	class Ground : public GameObject {
		Vec3 m_Scale;
		Vec3 m_Rotation;
		Vec3 m_Position;
	public:
		//構築と破棄
		Ground(const shared_ptr<Stage>& StagePtr,
			const Vec3& Scale,
			const Vec3& Rotation,
			const Vec3& Position
		);
		virtual ~Ground();
		//初期化
		virtual void OnCreate() override;
	};

	class Wall : public ObjectBase {
	public:
		//構築と破棄
		Wall(const shared_ptr<Stage>& StagePtr,
			const Vec3& Scale,
			const Vec3& Rotation,
			const Vec3& Position,
			const Vec3& ModelScale,
			const Vec3& ModelPosition
		) :
			ObjectBase(StagePtr, 
				Scale,
				Rotation,
				Position,
				ModelScale,
				ModelPosition
			)
		{}						
								
		virtual ~Wall();
		//初期化
		virtual void OnCreate() override;
	};

}
