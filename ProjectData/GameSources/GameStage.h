/*!
@file GameStage.h
@brief ゲームステージ
*/

#pragma once
#include "stdafx.h"
#include "ObjectBase.h"
#include "Building.h"

namespace basecross {
	enum EffectNum {
		efk_Explosion,
		efk_Hakai,
		efk_Smoke
	};

	//--------------------------------------------------------------------------------------
	//	ゲームステージクラス
	//--------------------------------------------------------------------------------------
	class GameStage : public GameSceneBase {
		shared_ptr<EfkInterface> m_efkInterface;
		vector<shared_ptr<EfkEffect>> m_efkEffect;
		vector<shared_ptr<EfkPlay>> m_efkPlay;

		vector<shared_ptr<Building>> m_FallBuilObject;
		unique_ptr<XmlDocReader> m_XmlDocReader;

		shared_ptr<SoundItem> m_bgm;

		int m_maxBreakObject;
		int m_brokenObject;
		int m_stageTime;
		float m_DestroyBuilTime;
		float m_MaxDestroyBuilTime;
		bool m_IsCountTime;
		bool m_gameCrearFlag = false;
		bool m_gamePlayeFlag;
		bool m_gameStart = false;
		bool m_gameFinish = false;

		//ビューの作成
		void CreateViewLight();
		void CreateEffect();
		void CreateGround();
		void CreatePlayer();
		void CreateUI();
	public:
		//構築と破棄
		GameStage() :GameSceneBase() {}
		virtual ~GameStage() {}
		//初期化
		virtual void OnCreate()override;

		//セッター、ゲッター
		shared_ptr<EfkInterface> GetEfkInterface() const {
			return m_efkInterface;
		}

		void SetMaxBreakObject(int num) {
			m_maxBreakObject = num;
		}

		int GetMaxBreakObject() {
			return m_maxBreakObject;
		}

		void SetBrokenObject(int num) {
			m_brokenObject = num;
		}

		int GetBrokenObject() {
			return m_brokenObject;
		}

		void SetGameCrearFlag(bool set) {
			m_gameCrearFlag = set;
		}

		void SetGamePlayeFlag(bool set) {
			m_gamePlayeFlag = set;
		}

		void SetGameStart(bool set) {
			m_gameStart = set;
		}

		bool GetGameStart() {
			return m_gameStart;
		}

		void SetGameFinish(bool set) {
			m_gameFinish = set;
		}

		bool GetGameFinish() {
			return m_gameFinish;
		}

		vector<shared_ptr<EfkEffect>> GetEfkEffect() {
			return m_efkEffect;
		}

		//処理
		virtual void OnUpdate() override;

		virtual void OnDraw() override;

		void AddMaxBreakObject(int num);

		void AddBrokenObject(int num);

		void AddFallBuilObject(shared_ptr<Building> builObject);

		void CreateEffectData();

		void DestroyBuilTime();

		void DestroyBuil();

		void WriteGameData();

		void ReadGameData();

		void AddEfkPlay(shared_ptr<EfkPlay> efkPlay);
	};
}
//end basecross

