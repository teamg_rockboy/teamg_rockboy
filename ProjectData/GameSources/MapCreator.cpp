/*!
@file MapCreator.cpp
@brief MapCreatorの実体
*/

#include "stdafx.h"
#include "Project.h"

namespace basecross {
	MapCreator::MapCreator(const shared_ptr<Stage>& StagePtr):
		GameObject(StagePtr)
	{}

	MapCreator::~MapCreator() {}

	void MapCreator::OnCreate() {
		auto ptrString = AddComponent<StringSprite>();
		ptrString->SetText(L"");
		ptrString->SetTextRect(Rect2D<float>(16.0f, 50.0f, 640.0f, 480.0f));

		m_height = 2.5f;

		//m_objectNameVec = {
		//	on_Wall,
		//	on_House,
		//	on_BuildingS,
		//	on_Water,
		//	on_GasTank,
		//	on_Slop,
		//	on_Bridge,
		//	on_Block,
		//	on_FreeHouse
		//};

		m_wallScale = Vec3(m_cellSize, m_cellSize * 2, m_cellSize);
		m_slopOfset = 1.8f;

		ResetObjectState();
		ReadXML();
		CreateMap();
		CreateObject();
	}

	void MapCreator::ResetObjectState() {
		m_objectState = {
			Vec3(m_cellSize / 1.3f),
			Vec3(0.0f),
			Vec3(0.0f),
			Vec3(1.0f),
			Vec3(0.0f,-0.5f,0.0f)
		};

		m_wallRotetonVec = {
			Vec3(0.0f,0.0f,0.0f),
			Vec3(0.0f,1.6f,0.0f),
			Vec3(0.0f,-1.6f,0.0f),
			Vec3(0.0f,3.15f,0.0f)
		};
	}

	//XML読み込み
	void MapCreator::ReadXML() {
		wstring DataDir;
		auto stageKey = App::GetApp()->GetScene<Scene>()->GetStageKey();
		wstring stageNode = L"MapData/" + stageKey;

		App::GetApp()->GetDataDirectory(DataDir);
		m_XmlDocReader.reset(new XmlDocReader(DataDir + L"XML/" + L"MapData.xml"));

		auto mapNode = m_XmlDocReader->GetSelectSingleNode(stageNode.c_str());
		wstring SetMapStr = XmlDocReader::GetText(mapNode);

		vector<wstring> LineVec;
		Util::WStrToTokenVector(LineVec, SetMapStr, L'\\');
		m_maxMapPos = { 0,0 };

		//�ｿｽ}�ｿｽb�ｿｽv�ｿｽf�ｿｽ[�ｿｽ^�ｿｽﾌ読み搾ｿｽ�ｿｽ�ｿｽ
		int count = 0;
		for (size_t i = 0; i < LineVec.size(); i++) {
			vector<wstring> Tokens;
			Util::WStrToTokenVector(Tokens, LineVec[i], L',');
			for (size_t j = 0; j < Tokens.size(); j++)
			{
				//
				if (Tokens[j] != L"|") {
					int num = stoi(Tokens[j]);
					m_mapData.push_back(num);
					count++;
				}
				else {
					m_maxMapPos[1]++;
				}
			}
		}
		m_maxMapPos[0] = count / m_maxMapPos[1];
	}

	//バイナリデータ書き出し
	void MapCreator::GetBinaryMapData() {
		double n = 10;
		fstream file;
		file.open("dataFile.txt", ios::out | ios::binary | ios::trunc);

		if (!file) {
			cout << "ga";
		}

		for (int i = 0; i < 2; i++) {
			file.write((char*)&n, sizeof(wstring));
			file.write((char*)"\n", sizeof(wstring));
		}
		file.close();
	}
	
	//マップデータのポジションを設定
	void MapCreator::CreateMap() {
		vector<int> mapRad = {
			m_maxMapPos[0] / 2,
			m_maxMapPos[1] / 2
		};


		vector<float> maxMapPos = {
			(float)mapRad[0] * -m_cellSize,
			(float)mapRad[1] * m_cellSize
		};

		vector<float> nowMapPos = maxMapPos;

		for (int i = 0; i < m_maxMapPos[1]; i++) {
			for (int j = 0; j < m_maxMapPos[0]; j++) {
				Vec3 setPos =  Vec3(nowMapPos[0], m_height, nowMapPos[1]);
				m_positionData.push_back(setPos);
				nowMapPos[0] += m_cellSize;
			}
			nowMapPos[1] -= m_cellSize;
			nowMapPos[0] = maxMapPos[0];
		}
	}

	//マップをもとに生成
	void MapCreator::CreateObject() {
		for (int i = 0; i < m_positionData.size(); i++) {
			m_objectState.Position = m_positionData[i];
			m_objectState.WaterPosition = Vec3(m_positionData[i].x, m_waterHeight, m_positionData[i].z);

			switch (m_mapData[i])
			{
			case on_Wall:
				ChooseWall(i);
				break;

			case on_House:
				GetStage()->AddGameObject<House>(
					m_objectState.Scale,
					m_objectState.Roteton,
					m_positionData[i],
					m_objectState.ModelScale,
					m_objectState.ModelPosition);
				AddMaxBreakObjectCount();
				break;

			case on_BuildingS:
				m_builPos = Vec3(m_positionData[i].x, m_builPos.y, m_positionData[i].z);
				GetStage()->AddGameObject<Building>(
					m_objectState.Scale,
					m_objectState.Roteton,
					m_builPos,
					m_objectState.ModelScale,
					m_builModelPos,
					100.0f);
				AddMaxBreakObjectCount();
				break;

			case on_GasTank:
				GetStage()->AddGameObject<GasTank>(
					m_objectState.Scale,
					m_objectState.Roteton,
					m_positionData[i],
					m_objectState.ModelScale,
					m_objectState.ModelPosition);
				AddMaxBreakObjectCount();
				break;

			case on_Slop:
				GetStage()->AddGameObject<Slop>(
					m_objectState.Scale,
					m_objectState.Roteton,
					Vec3(m_positionData[i].x, m_positionData[i].y - m_slopOfset, m_positionData[i].z),
					m_objectState.ModelScale,
					m_builModelPos);
				break;

			case on_VisibleSlop:
				GetStage()->AddGameObject<VisibleSlop>(
					m_objectState.Scale,
					Vec3(0.0f, 0.0f, -0.8f),
					Vec3(m_positionData[i].x, m_positionData[i].y - m_slopOfset, m_positionData[i].z),
					m_objectState.ModelScale,
					m_builModelPos);
				break;
			case on_Bridge:
				GetStage()->AddGameObject<Bridge>(
					m_objectState.Scale,
					Vec3(0.0f,0.0f,1.55f),
					m_positionData[i],
					m_objectState.ModelScale,
					m_builModelPos);
				break;

			case on_Block:
				GetStage()->AddGameObject<Block>(
					Vec3(m_cellSize),
					m_objectState.Roteton,
					m_objectState.Position
					);
				break;

			case on_WallCornerRightUp:
				GetStage()->AddGameObject<Wall_Corner>(
					m_wallScale,
					m_wallRotetonVec[0],
					m_objectState.Position,
					m_objectState.ModelScale,
					m_objectState.ModelPosition
					);
				break;

			case on_WallCornerLeftUp:
				GetStage()->AddGameObject<Wall_Corner>(
					m_wallScale,
					m_wallRotetonVec[1],
					m_objectState.Position,
					m_objectState.ModelScale,
					m_objectState.ModelPosition
					);
				break;

			case on_WallCornerRightDown:
				GetStage()->AddGameObject<Wall_Corner>(
					m_wallScale,
					m_wallRotetonVec[2],
					m_objectState.Position,
					m_objectState.ModelScale,
					m_objectState.ModelPosition
					);
				break;

			case on_WallCornerLeftDown:
				GetStage()->AddGameObject<Wall_Corner>(
					m_wallScale,
					m_wallRotetonVec[3],
					m_objectState.Position,
					m_objectState.ModelScale,
					m_objectState.ModelPosition
					);
				break;

			case on_Water:
				GetStage()->AddGameObject<Water>(
					Vec3(m_cellSize,0.5f,m_cellSize),
					m_objectState.Roteton,
					m_objectState.WaterPosition
					);
				break;

			case on_Restriction:
				GetStage()->AddGameObject<Restriction>(
					Vec3(m_cellSize,20.0f, m_cellSize),
					m_objectState.Roteton,
					m_objectState.Position
					);
				break;

			case on_FreeHouse:

				break;

			default:
				break;
			}
		}
	}

	void MapCreator::CreateTile(int itr) {

		for (int i = 0; i < m_positionData.size(); i++) {
			m_objectTile = {
				nullptr,

				nullptr,

				GetStage()->AddGameObject<House>(
					m_objectState.Scale,m_objectState.Roteton,m_positionData[i],
					m_objectState.ModelScale,m_objectState.ModelPosition),

				GetStage()->AddGameObject<Building>(
					m_objectState.Scale,m_objectState.Roteton,m_builPos,
					m_objectState.ModelScale,m_builModelPos,
					100.0f),

				nullptr,

				nullptr,

				GetStage()->AddGameObject<GasTank>(
					m_objectState.Scale,m_objectState.Roteton,m_positionData[i],
					m_objectState.ModelScale,m_objectState.ModelPosition),

				GetStage()->AddGameObject<Slop>(
					m_objectState.Scale,Vec3(-0.8f, 0.0f, 0.0f),
					Vec3(m_positionData[i].x, m_positionData[i].y - m_slopOfset, m_positionData[i].z),
					m_objectState.ModelScale,m_builModelPos),

				GetStage()->AddGameObject<Bridge>(
					m_objectState.Scale,Vec3(0.0f,0.0f,1.55f),m_positionData[i],
					m_objectState.ModelScale,m_builModelPos)
			};

			//m_objectTile[i];
		}
	}

	void MapCreator::ChooseWall(int num) {
		int itrUp = num - m_maxMapPos[0];
		int itrRight = num + 1;
		int itrLeft = num - 1;

		bool wallCorner_Up = false;
			wallCorner_Up =
				m_mapData[itrUp] == (int)on_WallCornerRightUp ||
				m_mapData[itrUp] == (int)on_WallCornerRightDown ||
				m_mapData[itrUp] == (int)on_WallCornerLeftUp ||
				m_mapData[itrUp] == (int)on_WallCornerLeftDown;
			wallCorner_Right =
				m_mapData[itrRight] == (int)on_WallCornerRightUp ||
				m_mapData[itrRight] == (int)on_WallCornerRightDown ||
				m_mapData[itrRight] == (int)on_WallCornerLeftUp ||
				m_mapData[itrRight] == (int)on_WallCornerLeftDown;
		}

		bool wallCorner_Left =
			m_mapData[itrLeft] == (int)on_WallCornerRightUp ||
			m_mapData[itrLeft] == (int)on_WallCornerRightDown ||
			m_mapData[itrLeft] == (int)on_WallCornerLeftUp ||
			m_mapData[itrLeft] == (int)on_WallCornerLeftDown;

		if (itrUp < 0) {
			CreateWall(w_Up);
		}
		else if (m_mapData[itrUp] == on_Wall || wallCorner_Up) {
			if (itrRight < m_mapData.size()) {
				if (m_mapData[itrRight] == on_Wall || wallCorner_Right) {
					CreateWall(w_Right);
				}
			}
			if (itrLeft > 0) {
				if (m_mapData[itrLeft] == on_Wall || wallCorner_Left) {
					CreateWall(w_Left);
				}
			}
		}
		else {
			CreateWall(w_Down);
		}
	}

	void MapCreator::CreateWall(int num) {
		GetStage()->AddGameObject<Wall>(
			m_wallScale,
			m_wallRotetonVec[num],
			m_objectState.Position,
			m_objectState.ModelScale,
			m_objectState.ModelPosition
			);
	}

	void MapCreator::AddMaxBreakObjectCount(){
		if (App::GetApp()->GetScene<Scene>()->GetNowStage() == L"Stage1") {
			GetTypeStage<GameStage>()->AddMaxBreakObject(1);
		}
	}

	void MapCreator::CreateHouse() {

	}

}
//end basecross
