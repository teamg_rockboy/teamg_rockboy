#include "stdafx.h"
#include "Project.h"

namespace basecross {
	void GameOverStage::CreateCamera() {
		auto PtrView = CreateView<SingleView>();
		//ビューのカメラの設定
		auto PtrCamera = ObjectFactory::Create<Camera>();
		PtrView->SetCamera(PtrCamera);
		PtrCamera->SetEye(0.0f, 0.0f, -10.0f);
		PtrCamera->SetAt(0.0f, 0.0f, 0.0f);
		//マルチライトの作成
		auto PtrMultiLight = CreateLight<MultiLight>();
		//デフォルトのライティングを指定
		PtrMultiLight->SetDefaultLighting();
	}

	void GameOverStage::CreateUI() {
											 
	}

	void GameOverStage::OnCreate() {
		try {
			//ビューとライトの作成
			CreateCamera();
			CreateUI();
		}
		catch (...) {
			throw;
		}
	}

	void GameOverStage::OnUpdate() {
		auto KeyState = App::GetApp()->GetInputDevice().GetKeyState();
		auto cntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		if (KeyState.m_bPressedKeyTbl['A'] || (cntlVec[0].fThumbLX < 0)) {
			m_select = 0;
		}
		else if (KeyState.m_bPressedKeyTbl['D'] || (cntlVec[0].fThumbLX > 0)) {
			m_select = 1;
		}
		if (((KeyState.m_bPressedKeyTbl[VK_SPACE]) || (cntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A)) && !m_select) {
			App::GetApp()->GetScene<Scene>()->SetStageState(StageState::title);
		}
		if (((KeyState.m_bPressedKeyTbl[VK_SPACE]) || (cntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A)) && m_select) {
			App::GetApp()->GetScene<Scene>()->SetStageState(StageState::game1);
		}
	}

}