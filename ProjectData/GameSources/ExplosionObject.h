/*!
@file MapCreator.h
@brief �j��̓����蔻��
*/

#pragma once
#include "stdafx.h"

namespace basecross {
	class ExplosionObject : public GameObject {
		Vec3 m_startPos;
		Vec3 m_startSca;
	public:
		ExplosionObject(const shared_ptr<Stage>& StagePtr,
			const Vec3& Position,
			const Vec3& Scale
		);

		virtual ~ExplosionObject() {};

		virtual void OnCreate()override;
		virtual void OnUpdate()override;

		void DestroyObject();
	};
}
//end basecross
