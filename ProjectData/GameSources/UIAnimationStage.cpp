#include "stdafx.h"
#include "Project.h"

namespace basecross {
	void UIStage::CreateCamera() {
		auto PtrView = CreateView<SingleView>();
		//ビューのカメラの設定
		auto PtrCamera = ObjectFactory::Create<Camera>();
		PtrView->SetCamera(PtrCamera);
		PtrCamera->SetEye(0.0f, 0.0f, -10.0f);
		PtrCamera->SetAt(0.0f, 0.0f, 0.0f);
		//マルチライトの作成
		auto PtrMultiLight = CreateLight<MultiLight>();
		//デフォルトのライティングを指定
		PtrMultiLight->SetDefaultLighting();
	}

	void UIStage::OnCreate() {
		try {
			CreateCamera();
		}
		catch (...) {
			throw;
		}
	}

	void UIStage::OnUpdate() {
	}

}