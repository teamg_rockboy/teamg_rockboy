
#include "stdafx.h"
#include "Project.h"

namespace basecross {

	//構築と破棄
	House::~House() {}

	//初期化
	void House::OnCreate() {
		auto ptrTrans = GetComponent<Transform>();
		SetTrans();
		SetModelName(L"m_House.bmf");
		ReadBmfData();
		SetEffectNum(efk_Smoke);
		SetEffectScale(Vec3(1.0f, 1.0f,1.0f));

		AddTag(L"House");

		auto ptrColl = AddComponent<CollisionObb>();
		ptrColl->SetMakedSize(Vec3(1.0f, 0.98f, 1.0f));

		ptrColl->SetUpdateActive(true);
		ptrColl->SetAfterCollision(AfterCollision::None);

		//物理計算ボックス
		PsBoxParam param(ptrTrans->GetWorldMatrix(), 100.0, true, PsMotionType::MotionTypeFixed);
		auto ptrPs = AddComponent<RigidbodyBox>(param);

	}

	void House::OnCollisionEnter(shared_ptr<GameObject>& Other) {
		if (Other->FindTag(L"Player") || Other->FindTag(L"Building") || Other->FindTag(L"ExpObj")) {
			this->SetDrawActive(false);
			auto ptrColl = GetComponent<CollisionObb>();
			ptrColl->SetUpdateActive(false);
			MyDesObject();
		}
	}
}