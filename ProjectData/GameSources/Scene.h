/*!
@file Scene.h
@brief シーン
*/
#pragma once

#include "stdafx.h"

namespace basecross{

	enum GameStageName {
		stage1,
		stage2,
		stage3
	};

	enum StageState {
		copyright,
		title,
		game1,
		debug,
		ui,
		result,
		mapselect,
		gameover,
		Max
	};

	//--------------------------------------------------------------------------------------
	///	ゲームシーン
	//--------------------------------------------------------------------------------------
	class Scene : public SceneBase{
		vector<wstring> m_stageName;
		
		void CreateResourses();
		void FindFile(wstring dir);

		void FindMediaDirectoryFile(wstring dir);
		wstring m_stageKey[StageState::Max];
		wstring m_nowStageKey = L"Stage1";
		wstring m_nowStage = L"Stage1";
		wstring m_selectGameStage = L"Stage1";
		vector<wstring> m_stageNameKey;
		vector<wstring> m_gameStageName;

		StageState m_stageState;
	public:
		//--------------------------------------------------------------------------------------
		/*!
		@brief コンストラクタ
		*/
		//--------------------------------------------------------------------------------------
		Scene();
		//--------------------------------------------------------------------------------------
		/*!
		@brief デストラクタ
		*/
		//--------------------------------------------------------------------------------------
		virtual ~Scene();
		//--------------------------------------------------------------------------------------
		/*!
		@brief 初期化
		@return	なし
		*/
		//--------------------------------------------------------------------------------------
		virtual void OnCreate() override;
		//--------------------------------------------------------------------------------------
		/*!
		@brief イベント取得
		@return	なし
		*/
		//--------------------------------------------------------------------------------------
		virtual void OnEvent(const shared_ptr<Event>& event) override;

		wstring GetStageKey() {
			return m_nowStageKey;
		}

		wstring GetNowStage() {
			return m_nowStage;
		}

		vector<wstring> GetGameStageName() {
			return m_gameStageName;
		}

		void SetSelectGameStage(int itr) {
			m_selectGameStage = m_gameStageName[itr];
		}

		int GetStageNum() {
			return (int)m_gameStageName.size();
		}

		
		//--------------------------------------------------------------------------------------
		/*!
		@brief イベント取得
		@return	なし
		*/
		//--------------------------------------------------------------------------------------
		void ResetStageName();

		void SetStageState(StageState key) {
			m_stageState = key;
			m_nowStageKey = m_stageNameKey[key];
			m_nowStage = m_stageNameKey[key];

			if (key == game1) {
				m_nowStageKey = m_selectGameStage;
			}

 			PostEvent(0.0f, GetThis<ObjectInterface>(), GetThis<Scene>(), m_stageKey[m_stageState]);
		}

	};

}

//end basecross
