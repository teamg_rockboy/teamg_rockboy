#include "stdafx.h"
#include "Project.h"
#define diff(x, y) ((x) > (y) ? ((x) - (y)) : ((y) - (x)))

namespace basecross {

	UI_Animation::UI_Animation(const shared_ptr<Stage>& StagePtr,
		const float& L,
		const float& R,
		const float& U,
		const float& D,
		const Vec3& pos,
		const Vec3& scale,
		const Vec3& rot,
		const int& Layer,
		const Col4& color,
		const wstring& Textures
	) :
		GameObject(StagePtr),
		m_Vertex_L(L),
		m_Vertex_R(R),
		m_Vertex_U(U),
		m_Vertex_D(D),
		m_pos(pos),
		m_firstPos(pos),
		m_scale(scale),
		m_firstScale(scale),
		m_rot(rot),
		m_firstRot(rot),
		m_Layer(Layer),
		m_color(color),
		m_firstColor(color),
		m_Textures(Textures),



		m_fullSize(0.0f),
		m_size(0.0f),
		m_colSize(0.0f),

		m_speedPos(0.0f),
		m_finishPos(0.0f),
		m_loopPos(0.0f),

		m_speedScale(0.0f),
		m_finishScale(0.0f),
		m_loopScale(0.0f),

		m_speedRot(0.0f),
		m_finishRot(0.0f),
		m_loopRot(0.0f),

		m_Vertex(0)

	{}
	UI_Animation::~UI_Animation() {}

	void UI_Animation::Draw() {
		Col4 color = Col4(1.0f, 1.0f, 1.0f, 1.0f);

		vector<VertexPositionColorTexture> vertices = {
			{ VertexPositionColorTexture(Vec3(m_Vertex_L,  m_Vertex_U, 0), color, Vec2(-0.0f, -0.0f)) },//左上
			{ VertexPositionColorTexture(Vec3(m_Vertex_R,  m_Vertex_U, 0), color, Vec2(1.0f, -0.0f)) },//右上
			{ VertexPositionColorTexture(Vec3(m_Vertex_L, m_Vertex_D, 0), color, Vec2(-0.0f,  1.0f)) },//左下
			{ VertexPositionColorTexture(Vec3(m_Vertex_R, m_Vertex_D, 0), color, Vec2(1.0f,  1.0f)) },//右下
		};

		vector<uint16_t> indices = { 0, 1, 2, 1, 3, 2 };

		SetAlphaActive(true);

		auto ptrDraw = AddComponent<PCTSpriteDraw>(vertices, indices);
		ptrDraw->SetTextureResource(m_Textures);
		ptrDraw->SetSamplerState(SamplerState::LinearWrap);

		ptrDraw->SetDiffuse(m_color);
		ptrDraw->SetEmissive(m_color);

		auto ptrTrans = GetComponent<Transform>();
		ptrTrans->SetPosition(m_pos);
		ptrTrans->SetScale(m_scale);
		ptrTrans->SetRotation(m_rot);
		SetDrawLayer(m_Layer);

	}

	void UI_Animation::VerteicesCreate() {
		m_firstVertex[0] = m_Vertex_L;
		m_firstVertex[1] = m_Vertex_R;
		m_firstVertex[2] = m_Vertex_U;
		m_firstVertex[3] = m_Vertex_D;

		m_Vertex_LenghtX = diff(m_Vertex_R, m_Vertex_L);
		m_Vertex_LenghtY = diff(m_Vertex_U, m_Vertex_D);


	}

	void UI_Animation::VerteicesUpdate(float size) {

		m_size = size;

		if (m_fullSize >= m_size) {
			switch (m_Vertex)
			{
			case 0:
				m_Vertex_L = m_firstVertex[0] + (m_Vertex_LenghtX - (m_Vertex_LenghtX / (m_fullSize / (m_fullSize - m_size))));
				break;
			case 1:
				m_Vertex_L = m_firstVertex[0] - (m_Vertex_LenghtX - (m_Vertex_LenghtX / (m_fullSize / (m_fullSize - m_size))));
				break;
			case 2:
				m_Vertex_R = m_firstVertex[1] + (m_Vertex_LenghtX - (m_Vertex_LenghtX / (m_fullSize / (m_fullSize - m_size))));
				break;
			case 3:
				m_Vertex_R = m_firstVertex[1] - (m_Vertex_LenghtX - (m_Vertex_LenghtX / (m_fullSize / (m_fullSize - m_size))));
				break;
			case 4:
				m_Vertex_U = m_firstVertex[2] + (m_Vertex_LenghtY - (m_Vertex_LenghtY / (m_fullSize / (m_fullSize - m_size))));
				break;
			case 5:
				m_Vertex_U = m_firstVertex[2] - (m_Vertex_LenghtY - (m_Vertex_LenghtY / (m_fullSize / (m_fullSize - m_size))));
				break;
			case 6:
				m_Vertex_D = m_firstVertex[3] + (m_Vertex_LenghtY - (m_Vertex_LenghtY / (m_fullSize / (m_fullSize - m_size))));
				break;
			case 7:
				m_Vertex_D = m_firstVertex[3] - (m_Vertex_LenghtY - (m_Vertex_LenghtY / (m_fullSize / (m_fullSize - m_size))));
				break;
			default:
				break;
			}
		}
		else {
			GetStage()->RemoveGameObject<GameObject>(GetThis<GameObject>());
		}

		vector<VertexPositionColorTexture> vertices = {
	{ VertexPositionColorTexture(Vec3(m_Vertex_L, m_Vertex_U, 0), Col4(1.0f, 1.0f, 1.0f, 1.0f), Vec2(-0.0f, -0.0f)) },//左上
	{ VertexPositionColorTexture(Vec3(m_Vertex_R, m_Vertex_U, 0), Col4(1.0f, 1.0f, 1.0f, 1.0f), Vec2(1.0f, -0.0f)) },//右上
	{ VertexPositionColorTexture(Vec3(m_Vertex_L, m_Vertex_D, 0), Col4(1.0f, 1.0f, 1.0f, 1.0f), Vec2(-0.0f,  1.0f)) },//左下
	{ VertexPositionColorTexture(Vec3(m_Vertex_R, m_Vertex_D, 0), Col4(1.0f, 1.0f, 1.0f, 1.0f), Vec2(1.0f,  1.0f)) },//右下
		};

		auto ptrDraw = GetComponent<PCTSpriteDraw>();
		ptrDraw->UpdateVertices(vertices);

	}

	void UI_Animation::VerteicesSetUpdate() {

		m_size += m_colSize;

		if (m_fullSize >= m_size) {
			switch (m_Vertex)
			{
			case 0:
				m_Vertex_L = m_firstVertex[0] + (m_Vertex_LenghtX - (m_Vertex_LenghtX / (m_fullSize / (m_fullSize - m_size))));
				break;
			case 1:
				m_Vertex_L = m_firstVertex[0] - (m_Vertex_LenghtX - (m_Vertex_LenghtX / (m_fullSize / (m_fullSize - m_size))));
				break;
			case 2:
				m_Vertex_R = m_firstVertex[1] + (m_Vertex_LenghtX - (m_Vertex_LenghtX / (m_fullSize / (m_fullSize - m_size))));
				break;
			case 3:
				m_Vertex_R = m_firstVertex[1] - (m_Vertex_LenghtX - (m_Vertex_LenghtX / (m_fullSize / (m_fullSize - m_size))));
				break;
			case 4:
				m_Vertex_U = m_firstVertex[2] + (m_Vertex_LenghtY - (m_Vertex_LenghtY / (m_fullSize / (m_fullSize - m_size))));
				break;
			case 5:
				m_Vertex_U = m_firstVertex[2] - (m_Vertex_LenghtY - (m_Vertex_LenghtY / (m_fullSize / (m_fullSize - m_size))));
				break;
			case 6:
				m_Vertex_D = m_firstVertex[3] + (m_Vertex_LenghtY - (m_Vertex_LenghtY / (m_fullSize / (m_fullSize - m_size))));
				break;
			case 7:
				m_Vertex_D = m_firstVertex[3] - (m_Vertex_LenghtY - (m_Vertex_LenghtY / (m_fullSize / (m_fullSize - m_size))));
				break;
			default:
				break;
			}
		}
		else {
			GetStage()->RemoveGameObject<GameObject>(GetThis<GameObject>());
		}

		vector<VertexPositionColorTexture> vertices = {
	{ VertexPositionColorTexture(Vec3(m_Vertex_L, m_Vertex_U, 0), Col4(1.0f, 1.0f, 1.0f, 1.0f), Vec2(-0.0f, -0.0f)) },//左上
	{ VertexPositionColorTexture(Vec3(m_Vertex_R, m_Vertex_U, 0), Col4(1.0f, 1.0f, 1.0f, 1.0f), Vec2(1.0f, -0.0f)) },//右上
	{ VertexPositionColorTexture(Vec3(m_Vertex_L, m_Vertex_D, 0), Col4(1.0f, 1.0f, 1.0f, 1.0f), Vec2(-0.0f,  1.0f)) },//左下
	{ VertexPositionColorTexture(Vec3(m_Vertex_R, m_Vertex_D, 0), Col4(1.0f, 1.0f, 1.0f, 1.0f), Vec2(1.0f,  1.0f)) },//右下
		};

		auto ptrDraw = GetComponent<PCTSpriteDraw>();
		ptrDraw->UpdateVertices(vertices);

	}

	void UI_Animation::TransformCreate() {
		m_movePos = m_finishPos - m_firstPos;
		m_moveScale = m_finishScale - m_firstScale;
		m_moveRot = m_finishRot - m_firstRot;
		m_moveColor = m_finishColor - m_firstColor;
	}

	void UI_Animation::TransformResetCreate() {
		auto trans = GetComponent<Transform>();
		auto pos = trans->GetPosition();
		auto scale = trans->GetScale();
		auto rot = trans->GetRotation();

		m_movePos = m_finishPos - pos;
		m_moveScale = m_finishScale - scale;
		m_moveRot = m_finishRot - rot;
		m_moveColor = m_finishColor - m_firstColor;
	}

	void UI_Animation::PositionUpdate() {

		if (m_loopPos) {
			if (m_movePos.x > 0) {
				if ((m_firstPos.x > m_pos.x) || (m_pos.x > m_finishPos.x)) {
					m_speedPos = -m_speedPos;
				}
			}
			else if (m_movePos.x < 0) {
				if ((m_firstPos.x < m_pos.x) || (m_pos.x < m_finishPos.x)) {
					m_speedPos = -m_speedPos;
				}
			}
			else if (m_movePos.y > 0) {
				if ((m_firstPos.y > m_pos.y) || (m_pos.y > m_finishPos.y)) {
					m_speedPos = -m_speedPos;
				}
			}
			else if (m_movePos.y < 0) {
				if ((m_firstPos.y < m_pos.y) || (m_pos.y < m_finishPos.y)) {
					m_speedPos = -m_speedPos;
				}
			}
			else if (m_movePos.z > 0) {
				if ((m_firstPos.z > m_pos.z) || (m_pos.z > m_finishPos.z)) {
					m_speedPos = -m_speedPos;
				}
			}
			else if (m_movePos.z < 0) {
				if ((m_firstPos.z < m_pos.z) || (m_pos.z < m_finishPos.z)) {
					m_speedPos = -m_speedPos;
				}
			}
			m_pos += m_movePos * m_speedPos;
		}
		else {
			if (m_movePos.x > 0) {
				if (m_pos.x <= m_finishPos.x) {
					m_pos += m_movePos * m_speedPos;
				}
			}
			else if (m_movePos.x < 0) {
				if (m_pos.x >= m_finishPos.x) {
					m_pos += m_movePos * m_speedPos;
				}
			}
			else if (m_movePos.y > 0) {
				if (m_pos.y <= m_finishPos.y) {
					m_pos += m_movePos * m_speedPos;
				}
			}
			else if (m_movePos.y < 0) {
				if (m_pos.y >= m_finishPos.y) {
					m_pos += m_movePos * m_speedPos;
				}
			}
			else if (m_movePos.z > 0) {
				if (m_pos.z <= m_finishPos.z) {
					m_pos += m_movePos * m_speedPos;
				}
			}
			else if (m_movePos.z < 0) {
				if (m_pos.z >= m_finishPos.z) {
					m_pos += m_movePos * m_speedPos;
				}
			}
		}

		GetComponent<Transform>()->SetPosition(m_pos);
	}

	void UI_Animation::ScaleUpdate() {
		if (m_loopScale) {
			if (m_moveScale.x > 0) {
				if ((m_firstScale.x > m_scale.x) || (m_scale.x > m_finishScale.x)) {
					m_speedScale = -m_speedScale;
				}
			}
			else if (m_moveScale.x < 0) {
				if ((m_firstScale.x < m_scale.x) || (m_scale.x < m_finishScale.x)) {
					m_speedScale = -m_speedScale;
				}
			}
			else if (m_moveScale.y > 0) {
				if ((m_firstScale.y > m_scale.y) || (m_scale.y > m_finishScale.y)) {
					m_speedScale = -m_speedScale;
				}
			}
			else if (m_moveScale.y < 0) {
				if ((m_firstScale.y < m_scale.y) || (m_scale.y < m_finishScale.y)) {
					m_speedScale = -m_speedScale;
				}
			}
			else if (m_moveScale.z > 0) {
				if ((m_firstScale.z > m_scale.z) || (m_scale.z > m_finishScale.z)) {
					m_speedScale = -m_speedScale;
				}
			}
			else if (m_moveScale.z < 0) {
				if ((m_firstScale.z < m_scale.z) || (m_scale.z < m_finishScale.z)) {
					m_speedScale = -m_speedScale;
				}
			}
			m_scale += m_moveScale * m_speedScale;
		}
		else {
			if (m_moveScale.x > 0) {
				if (m_scale.x <= m_finishScale.x) {
					m_scale += m_moveScale * m_speedScale;
				}
			}
			else if (m_moveScale.x < 0) {
				if (m_scale.x >= m_finishScale.x) {
					m_scale += m_moveScale * m_speedScale;
				}
			}
			else if (m_moveScale.y > 0) {
				if (m_scale.y <= m_finishScale.y) {
					m_scale += m_moveScale * m_speedScale;
				}
			}
			else if (m_moveScale.y < 0) {
				if (m_scale.y >= m_finishScale.y) {
					m_scale += m_moveScale * m_speedScale;
				}
			}
			else if (m_moveScale.z > 0) {
				if (m_scale.z <= m_finishScale.z) {
					m_scale += m_moveScale * m_speedScale;
				}
			}
			else if (m_moveScale.z < 0) {
				if (m_scale.z >= m_finishScale.z) {
					m_scale += m_moveScale * m_speedScale;
				}
			}
		}

		GetComponent<Transform>()->SetScale(m_scale);
	}

	void UI_Animation::RotationUpdate() {
		if (m_loopRot) {
			if (m_moveRot.x > 0) {
				if ((m_firstRot.x > m_rot.x) || (m_rot.x > m_finishRot.x)) {
					m_speedRot = -m_speedRot;
				}
			}
			else if (m_moveRot.x < 0) {
				if ((m_firstRot.x < m_rot.x) || (m_rot.x < m_finishRot.x)) {
					m_speedRot = -m_speedRot;
				}
			}
			else if (m_moveRot.y > 0) {
				if ((m_firstRot.y > m_rot.y) || (m_rot.y > m_finishRot.y)) {
					m_speedRot = -m_speedRot;
				}
			}
			else if (m_moveRot.y < 0) {
				if ((m_firstRot.y < m_rot.y) || (m_rot.y < m_finishRot.y)) {
					m_speedRot = -m_speedRot;
				}
			}
			else if (m_moveRot.z > 0) {
				if ((m_firstRot.z > m_rot.z) || (m_rot.z > m_finishRot.z)) {
					m_speedRot = -m_speedRot;
				}
			}
			else if (m_moveRot.z < 0) {
				if ((m_firstRot.z < m_rot.z) || (m_rot.z < m_finishRot.z)) {
					m_speedRot = -m_speedRot;
				}
			}
			m_rot += m_moveRot * m_speedRot;
		}
		else if (m_infRot) {
			m_rot += m_moveRot * m_speedRot;
		}
		else {
			if (m_moveRot.x > 0) {
				if (m_rot.x <= m_finishRot.x) {
					m_rot += m_moveRot * m_speedRot;
				}
			}
			else if (m_moveRot.x < 0) {
				if (m_rot.x >= m_finishRot.x) {
					m_rot += m_moveRot * m_speedRot;
				}
			}
			else if (m_moveRot.y > 0) {
				if (m_rot.y <= m_finishRot.y) {
					m_rot += m_moveRot * m_speedRot;
				}
			}
			else if (m_moveRot.y < 0) {
				if (m_rot.y >= m_finishRot.y) {
					m_rot += m_moveRot * m_speedRot;
				}
			}
			else if (m_moveRot.z > 0) {
				if (m_rot.z <= m_finishRot.z) {
					m_rot += m_moveRot * m_speedRot;
				}
			}
			else if (m_moveRot.z < 0) {
				if (m_rot.z >= m_finishRot.z) {
					m_rot += m_moveRot * m_speedRot;
				}
			}
		}

		GetComponent<Transform>()->SetRotation(m_rot);
	}

	void UI_Animation::ColorUpdate() {
		auto ptrDraw = GetComponent<PCTSpriteDraw>();

		if (m_loopColor) {
			if (m_moveColor.w > 0) {
				if ((m_color.w > m_finishColor.w) || (m_color.w < m_firstColor.w)) {
					m_speedColor.w = -m_speedColor.w;
				}
			}
			else if (m_moveColor.w < 0) {
				if ((m_color.w < m_finishColor.w) || (m_color.w > m_firstColor.w)) {
					m_speedColor.w = -m_speedColor.w;
				}
			}
			m_color.w += m_moveColor.w * -m_speedColor.w;
		}
		else if (((m_color.w > m_finishColor.w) || (m_color.w < m_firstColor.w)) ||
			((m_color.w < m_finishColor.w) || (m_color.w > m_firstColor.w))) {
			m_color.w += m_moveColor.w * m_speedColor.w;
		}


		//色の変化
		ptrDraw->SetDiffuse(m_color);
		ptrDraw->SetEmissive(m_color);

	}

	float UI_Animation::TimeUpdate() {
		float elapsedTime = App::GetApp()->GetElapsedTime();
		m_time -= elapsedTime;

		return m_time;
	}



	void UI_GameStage::OnCreate() {
		Draw();
	}

	void GageBar::OnCreate() {
		Draw();
		SetVertex(0, (float)GetTypeStage<GameStage>()->GetMaxBreakObject(), 0);
		VerteicesCreate();

	}

	void GageBar::OnUpdate() {
		VerteicesUpdate((float)GetTypeStage<GameStage>()->GetBrokenObject());
	}

	void UI_GameReady::OnCreate() {
		SetTime(4.0f);
	}

	void UI_GameReady::OnUpdate() {
		float time = TimeUpdate();
		ScaleUpdate();
		if (time > 0.0f) {
			if (time <= 3.0f && m_draw == 0) {
				Draw();
				SetScale(1.0f / 60.0f, Vec3(1.0f, 1.0f, 1.0f), false);
				TransformCreate();
				m_draw++;
			}
			else if (time <= 2.0f && m_draw == 1) {
				GetStage()->AddGameObject<UI_GameStart>(
					-800.0f,
					800.0f,
					200.0f,
					-200.0f,
					Vec3(0.0f, -150.0f, 0.0f),
					Vec3(0.01f, 0.01f, 0.01f),
					Vec3(0.0f, 0.0f, 0.0f),
					10,
					Col4(1.0f, 1.0f, 1.0f, 1),
					L"Text_Start.png"
					);
				m_draw++;
			}
		}
		else {
			GetTypeStage<GameStage>()->SetGameStart(true);
			GetStage()->RemoveGameObject<GameObject>(GetThis<GameObject>());
		}

	}

	void UI_GameStart::OnCreate() {
		Draw();
		SetTime(2.0f);
		SetScale(1.0f / 60.0f, Vec3(1.0f, 1.0f, 1.0f), false);
		TransformCreate();

		auto soundMane = App::GetApp()->GetXAudio2Manager();
		m_se_UI = soundMane->Start(L"whistle02.wav", 0, 0.2f);
	}

	void UI_GameStart::OnUpdate() {
		ScaleUpdate();
		if (TimeUpdate() <= 0) {
			auto soundMane = App::GetApp()->GetXAudio2Manager();
			soundMane->Stop(m_se_UI);

			GetStage()->RemoveGameObject<GameObject>(GetThis<GameObject>());
		}
	}

	void UI_GameFinish::OnCreate() {
		Draw();
		SetTime(2.0f);
		SetPosition(0.05f, Vec3(200.0f, 0.0f, 0.0f), false);
		TransformCreate();

		auto soundMane = App::GetApp()->GetXAudio2Manager();
		m_se_UI = soundMane->Start(L"whistle01.wav", 0, 0.2f);
	}

	void UI_GameFinish::OnUpdate() {
		float time = TimeUpdate();

		if (time > 0) {
			if (time < 1.5f && m_draw == 0) {
				SetPosition(0.001f, Vec3(-200.0f, 0.0f, 0.0f), false);
				TransformCreate();
				m_draw++;
			}
			else if (time < 0.5f && m_draw == 1)
			{
				SetPosition(0.05f, Vec3(-1700.0f, 0.0f, 0.0f), false);
				TransformCreate();
				m_draw++;
			}
		}
		else {
			GetTypeStage<GameStage>()->WriteGameData();
			auto soundMane = App::GetApp()->GetXAudio2Manager();
			soundMane->Stop(m_se_UI);

			GetStage()->AddGameObject<UI_RockBoy>(
				true,
				3
				);
			GetStage()->RemoveGameObject<GameObject>(GetThis<GameObject>());
		}

		PositionUpdate();
	}



	void UI_RockBoy::OnCreate() {
		Vec3 pos = Vec3(0.0f, 0.0f, 0.0f);
		Vec3 rot = Vec3(0.0f, 0.0f, 0.0f);
		if (m_switch) {
			pos = Vec3(3000.0f, 0.0f, 0.0f);
			rot = Vec3(0.0f, 0.0f, -3.5f);
		}
		GetStage()->AddGameObject<UI_RockBoyOrigin>(
			-1200.0f,
			1200.0f,
			1200.0f,
			-1200.0f,
			pos,
			Vec3(1.0f, 1.0f, 1.0f),
			rot,
			100,
			Col4(1.0f, 1.0f, 1.0f, 1),
			L"RockBoy.png",
			m_switch,
			m_scene
			);
	}

	void UI_RockBoyOrigin::OnCreate() {

		if (m_switch) {
			EndCreate();
		}
		else {
			BeginCreate();
		}

		Draw();

		SetTime(1.25f);
		TransformCreate();

	}

	void UI_RockBoyOrigin::OnUpdate() {
		PositionUpdate();
		RotationUpdate();
		if (TimeUpdate() <= 0) {
			if (m_switch) {
				EndUpdate();
			}
			else {
				BeginUpdate();
			}
		}
	}

	void UI_RockBoyOrigin::BeginCreate() {
		SetPosition(1.0f / 75.0f, Vec3(-3000.0f, 0.0f, 0.0f), false);
		SetRotation(1.0f / 75.0f, Vec3(0.0f, 0.0f, 3.5f), false, false);
	}

	void UI_RockBoyOrigin::EndCreate() {
		SetPosition(1.0f / 75.0f, Vec3(0.0f, 0.0f, 0.0f), false);
		SetRotation(1.0f / 75.0f, Vec3(0.0f, 0.0f, 0.0f), false, false);

		auto soundMane = App::GetApp()->GetXAudio2Manager();
		m_se_UI = soundMane->Start(L"kan_ge_dosya01.wav", 0, 0.2f);
	}

	void UI_RockBoyOrigin::BeginUpdate() {
		switch (m_scene)
		{
		case 1:
			GetStage()->AddGameObject<UI_TitleLogo>();
			break;
		case 2:
			GetTypeStage<ResultStage>()->SetCountStart(true);
			break;
		case 3:
			GetStage()->AddGameObject<UI_GameReady>(
				-800.0f,
				800.0f,
				200.0f,
				-200.0f,
				Vec3(0.0f, 150.0f, 0.0f),
				Vec3(0.01f, 0.01f, 0.01f),
				Vec3(0.0f, 0.0f, 0.0f),
				10,
				Col4(1.0f, 1.0f, 1.0f, 1),
				L"Text_ready.png"
				);
			break;
		default:
			break;
		}

		GetStage()->RemoveGameObject<GameObject>(GetThis<GameObject>());
	}

	void UI_RockBoyOrigin::EndUpdate() {
		auto soundMane = App::GetApp()->GetXAudio2Manager();
		soundMane->Stop(m_se_UI);

		switch (m_scene)
		{
		case 0:
			App::GetApp()->GetScene<Scene>()->SetStageState(StageState::title);
			break;
		case 1:
			App::GetApp()->GetScene<Scene>()->SetStageState(StageState::mapselect);
			break;
		case 2:
			App::GetApp()->GetScene<Scene>()->SetStageState(StageState::game1);
			break;
		case 3:
			App::GetApp()->GetScene<Scene>()->SetStageState(StageState::result);
			break;
		default:
			break;
		}
	}



	void UI_TitleLogo::OnCreate() {
		GetStage()->AddGameObject<UI_TitleLogoA>(
			-1000.0f,
			1000.0f,
			500.0f,
			-500.0f,
			Vec3(-1200.0f, 0.0f, 0.0f),
			Vec3(1.0f, 1.0f, 1.0f),
			Vec3(0.0f, 0.0f, 0.0f),
			5,
			Col4(0.7f, 0.7f, 0.7f, 1),
			L"TitleLogoA.png"
			);
		GetStage()->AddGameObject<UI_TitleLogoB>(
			-1000.0f,
			1000.0f,
			500.0f,
			-500.0f,
			Vec3(1200.0f, 0.0f, 0.0f),
			Vec3(1.0f, 1.0f, 1.0f),
			Vec3(0.0f, 0.0f, 0.0f),
			4,
			Col4(0.7f, 0.7f, 0.7f, 1),
			L"TitleLogoB.png"
			);
	}

	void UI_TitleLogoA::OnCreate() {
		Draw();
		SetTime(0.25f);
		SetPosition(1.0f / 15.0, Vec3(-165.0f, 115.0f, 0.0f), false);
		TransformCreate();
	}

	void UI_TitleLogoA::OnUpdate() {
		PositionUpdate();
		if (TimeUpdate() <= 0.0f) {
			GetStage()->RemoveGameObject<UI_TitleLogoA>(GetThis<UI_TitleLogoA>());
		}
		
	}

	void UI_TitleLogoB::OnCreate() {
		Draw();
		SetTime(0.25f);
		SetPosition(1.0f / 15.0f, Vec3(490.0f, 140.0f, 0.0f), false);
		TransformCreate();

	}

	void UI_TitleLogoB::OnUpdate() {
		PositionUpdate();
		if (TimeUpdate() <= 0.0f) {
			GetStage()->AddGameObject<UI_TitleLogoAB>(
				-1000.0f,
				1000.0f,
				500.0f,
				-500.0f,
				Vec3(0.0f, 0.0f, 0.0f),
				Vec3(1.0f, 1.0f, 1.0f),
				Vec3(0.0f, 0.0f, 0.0f),
				4,
				Col4(0.5f, 0.5f, 0.5f, 1),
				L"TitleLogoABC.png"
				);
			GetStage()->RemoveGameObject<UI_TitleLogoB>(GetThis<UI_TitleLogoB>());
		}
	}

	void UI_TitleLogoAB::OnCreate() {
		Draw();
		SetTime(0.5f);
		SetScale(1.0f / 10.0f, Vec3(0.1f, 1.0f, 1.0f), false);
		TransformCreate();
	}

	void UI_TitleLogoAB::OnUpdate() {
		ScaleUpdate();
		if (TimeUpdate() <= 0.0f && m_draw == 0) {
			m_draw++;
			SetTime(0.5f);
			SetScale(1.0f / 10.0f, Vec3(0.91f, 1.0f, 1.0f), false);
			TransformResetCreate();
		}
		else if (TimeUpdate() <= 0 && m_draw == 1)
		{
			m_draw++;
			SetTime(0.65f);
			GetStage()->AddGameObject<UI_TitleLogoC>(
				-1000.0f,
				1000.0f,
				500.0f,
				-500.0f,
				Vec3(0.0f, -300.0f, 0.0f),
				Vec3(1.0f, 1.0f, 1.0f),
				Vec3(0.0f, 0.0f, 0.0f),
				5,
				Col4(0.7f, 0.7f, 0.7f, 1),
				L"TitleLogoD.png"
				);
		}
		else if (TimeUpdate() <= 0 && m_draw == 2)
		{
			GetStage()->RemoveGameObject<UI_TitleLogoAB>(GetThis<UI_TitleLogoAB>());
		}
	}

	void UI_TitleLogoC::OnCreate() {
		Draw();
		SetTime(0.15f);
		SetPosition(1.0f / 10.0f, Vec3(0.0f, -0.1f, 0.0f), false);
		TransformCreate();
	}

	void UI_TitleLogoC::OnUpdate() {
		PositionUpdate();
		if (TimeUpdate() <= 0) {
			GetStage()->AddGameObject<UI_TitleLogoD>(
				-1000.0f,
				1000.0f,
				500.0f,
				-500.0f,
				Vec3(0.0f, 0.0f, 0.0f),
				Vec3(1.0f, 1.0f, 1.0f),
				Vec3(0.0f, 0.0f, 0.0f),
				10,
				Col4(0.5f, 0.5f, 0.5f, 1),
				L"TitleLogo.png"
				);
			GetStage()->RemoveGameObject<UI_TitleLogoC>(GetThis<UI_TitleLogoC>());
		}
	}

	void UI_TitleLogoD::OnCreate() {
		Draw();
		GetStage()->AddGameObject<UI_TitleAPush>(
			-512.0f,
			512.0f,
			128.0f,
			-128.0f,
			Vec3(0.0f, -400.0f, 0.0f),
			Vec3(0.6f, 0.6f, 1.0f),
			Vec3(0.0f, 0.0f, 0.0f),
			5,
			Col4(1.0f, 1.0f, 1.0f, 1),
			L"Text_AButtonPush.png"
			);
	}

	void UI_TitleAPush::OnCreate() {
		Draw();
		SetColor(Col4(0.0f, 0.0f, 0.0f, 0.02f), Col4(1.0f, 1.0f, 1.0f, 0.4f), true);
		TransformCreate();
	}

	void UI_TitleAPush::OnUpdate() {
		ColorUpdate();
	}



	void UI_MapSelect::OnCreate() {
		Draw();
		SetScale(0.05f, Vec3(0.85f, 0.85f, 1.0f), true);
		TransformCreate();
	}

	void UI_MapSelect::OnUpdate()
	{
		ScaleUpdate();
	}

	void UI_MapTextSelect::OnCreate() {
		Draw();
	}

	void UI_MapPicSelect::OnCreate() {
		Draw();
		m_layer = GetLayer();
		m_color = GetColor();
		m_text[0] = L"Text_Stage1.png";
		m_text[1] = L"Text_Stage2.png";
		m_text[2] = L"Text_Stage3.png";
		m_text[3] = L"Text_Stage4.png";
		m_text[4] = L"Text_Stage5.png";
		m_text[5] = L"Text_Stage6.png";
		m_text[6] = L"Text_Stage7.png";
		m_text[7] = L"Text_Stage8.png";
		m_text[8] = L"Text_Stage9.png";
		m_text[9] = L"Text_Stage10.png";
	}

	void UI_MapPicSelect::OnUpdate() {
		SelectMove();
		Col4 color = Col4(0.2f, 0.2f, 0.2f, 1.0f);

		int textrues = m_layer + m_select;

		if (textrues >= m_textures) {
			textrues = textrues - m_textures;
		}

		if (textrues < 0) {
			textrues = textrues + m_textures;
		}

		if (m_layer == 0) {
			color = m_color;
		}

		GetTypeStage<MapSelectStage>()->SetSelect(m_select);

		auto ptrDraw = GetComponent<PCTSpriteDraw>();
		ptrDraw->SetTextureResource(m_text[textrues]);
		
		ptrDraw->SetDiffuse(color);
		ptrDraw->SetEmissive(color);

	}

	void UI_MapPicSelect::SelectMove() {
		auto KeyState = App::GetApp()->GetInputDevice().GetKeyState();
		auto cntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		if (KeyState.m_bPressedKeyTbl['A'] || ((cntlVec[0].fThumbLX < -0.1f) && m_stick)) {
			if (m_select == 0) {
				m_select = m_textures-1;
			}
			else {
				m_select--;
			}
			m_stick = false;

			auto xAPtr = App::GetApp()->GetXAudio2Manager();
			xAPtr->Start(L"se_maoudamashii_system24.wav", 0, 0.2f);
		}
		else if (KeyState.m_bPressedKeyTbl['D'] || ((cntlVec[0].fThumbLX > 0.1f) && m_stick)) {
			if (m_select == m_textures - 1) {
				m_select = 0;
			}
			else {
				m_select++;
			}
			m_stick = false;

			auto xAPtr = App::GetApp()->GetXAudio2Manager();
			xAPtr->Start(L"se_maoudamashii_system24.wav", 0, 0.2f);
		}
		else if ((cntlVec[0].fThumbLX > -0.1f) && (cntlVec[0].fThumbLX < 0.1f)) {
			m_stick = true;
		}

		if (m_scene && ((KeyState.m_bPressedKeyTbl[VK_SPACE]) || (cntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A))) {
			auto scene = App::GetApp()->GetScene<Scene>();
			scene->SetSelectGameStage(m_select);
			GetStage()->AddGameObject<UI_RockBoy>(
				true,
				2
				);

			m_scene = false;

			auto xAPtr = App::GetApp()->GetXAudio2Manager();
			xAPtr->Start(L"se_maoudamashii_system24.wav", 0, 0.2f);
		}
	}

	void UI_MapBackSelect::OnCreate() {
		Draw();
		m_textures[0] = L"StageBack1.png";
		m_textures[1] = L"StageBack2.png";
		m_textures[2] = L"StageBack3.png";
		m_textures[3] = L"StageBack4.png";
		m_textures[4] = L"StageBack5.png";
		m_textures[5] = L"StageBack6.png";
		m_textures[6] = L"StageBack7.png";
		m_textures[7] = L"StageBack8.png";
		m_textures[8] = L"StageBack9.png";
		m_textures[9] = L"StageBack10.png";

	}

	void UI_MapBackSelect::OnUpdate() {
		int select = GetTypeStage<MapSelectStage>()->GetSelect();

		auto ptrDraw = GetComponent<PCTSpriteDraw>();

		ptrDraw->SetTextureResource(m_textures[select]);

	}




	void UI_ResultLogo::OnCreate() {
		Draw();
	}

	void UI_ResultSelect::OnCreate() {
		Draw();
		SetColor(Col4(0.0f, 0.0f, 0.0f, 0.01f), Col4(1.0f, 1.0f, 1.0f, 0.3f), true);
		TransformCreate();
	}

	void UI_ResultSelect::OnUpdate() {
		auto ptrTrans = GetComponent<Transform>()->GetPosition();
		auto KeyState = App::GetApp()->GetInputDevice().GetKeyState();
		auto cntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		if (KeyState.m_bPressedKeyTbl['A'] || ((cntlVec[0].fThumbLX < -0.4f) && m_select)) {
			m_sceneMove = 0;
			m_pos.x = -250.0f;

			auto xAPtr = App::GetApp()->GetXAudio2Manager();
			xAPtr->Start(L"se_maoudamashii_system24.wav", 0, 0.2f);
			m_select = false;
		}
		else if (KeyState.m_bPressedKeyTbl['D'] || ((cntlVec[0].fThumbLX > 0.4f) && m_select)) {
			m_pos.x = 250.0f;
			m_sceneMove = 2;

			auto xAPtr = App::GetApp()->GetXAudio2Manager();
			xAPtr->Start(L"se_maoudamashii_system24.wav", 0, 0.2f);
			m_select = false;
		}
		else if ((cntlVec[0].fThumbLX < 0.4f) && (cntlVec[0].fThumbLX > -0.4f)) {
			m_select = true;
		}
		ptrTrans = m_pos;
		GetComponent<Transform>()->SetPosition(ptrTrans);

		ColorUpdate();

		if (((KeyState.m_bPressedKeyTbl[VK_SPACE]) || (cntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A)) && m_scene) {
			GetStage()->AddGameObject<UI_RockBoy>(
				true,
				m_sceneMove
				);
			m_scene = false;
		}

	}

	void UI_ResultGageBar::OnCreate() {
		Draw();
		SetVertex(0, (float)GetTypeStage<ResultStage>()->GetMaxBrokenObject(), 0);
		VerteicesCreate();
		m_maxCount = (float)GetTypeStage<ResultStage>()->GetBrokenObject();

	}

	void UI_ResultGageBar::OnUpdate() {
		bool start = GetTypeStage<ResultStage>()->GetCountStart();

		if (m_maxCount != m_count && start) {
			m_count++;
			VerteicesUpdate(m_count);
		}
	}

	void UI_Result::OnCreate() {
		GetStage()->AddGameObject<UI_ResultMaxCount>(
			3,
			L"Text_Number.png",
			Vec3(150.0f, 50.0f, 0.0f),
			51
			);

		GetStage()->AddGameObject<UI_ResultLogo>(
			-50.0f,
			50.0f,
			60.0f,
			-60.0f,
			Vec3(0.0f, 50.0f, 0.0f),
			Vec3(1.0f, 1.0f, 1.0f),
			Vec3(0.0f, 0.0f, 0.0f),
			10,
			Col4(1.0f, 1.0f, 1.0f, 1),
			L"Slash.png"
			);

		GetStage()->AddGameObject<UI_ResultCount>(
			3,
			L"Text_Number.png",
			Vec3(-150.0f, 50.0f, 0.0f),
			51
			);
	}



	UI_ResultCount::UI_ResultCount(const shared_ptr<Stage>& StagePtr, UINT NumberOfDigits,
		const wstring& TextureKey,
		const Vec3& StartPos,
		const int& Layer) :
		GameObject(StagePtr),
		m_NumberOfDigits(NumberOfDigits),
		m_TextureKey(TextureKey),
		m_StartPos(StartPos),
		m_Layer(Layer)
	{}

	void UI_ResultCount::OnCreate() {
		float xPiecesize = 1.0f / (float)m_NumberOfDigits;
		float helfSize = 0.5f;

		//インデックス配列
		vector<uint16_t> indices;
		for (UINT i = 0; i < m_NumberOfDigits; i++) {
			float vertex0 = -helfSize + xPiecesize * (float)i;
			float vertex1 = vertex0 + xPiecesize;
			//0
			m_BackupVertices.push_back(
				VertexPositionTexture(Vec3(vertex0, helfSize, 0), Vec2(0.0f, 0.0f))
			);
			//1
			m_BackupVertices.push_back(
				VertexPositionTexture(Vec3(vertex1, helfSize, 0), Vec2(0.1f, 0.0f))
			);
			//2
			m_BackupVertices.push_back(
				VertexPositionTexture(Vec3(vertex0, -helfSize, 0), Vec2(0.0f, 1.0f))
			);
			//3
			m_BackupVertices.push_back(
				VertexPositionTexture(Vec3(vertex1, -helfSize, 0), Vec2(0.1f, 1.0f))
			);
			indices.push_back(i * 4 + 0);
			indices.push_back(i * 4 + 1);
			indices.push_back(i * 4 + 2);
			indices.push_back(i * 4 + 1);
			indices.push_back(i * 4 + 3);
			indices.push_back(i * 4 + 2);
		}

		SetAlphaActive(true);
		auto ptrTrans = GetComponent<Transform>();
		ptrTrans->SetScale(Vec3(180.0f, 120.0f, 1.0f));
		ptrTrans->SetPosition(m_StartPos);
		//頂点とインデックスを指定してスプライト作成
		auto ptrDraw = AddComponent<PTSpriteDraw>(m_BackupVertices, indices);
		ptrDraw->SetTextureResource(m_TextureKey);
		GetStage()->SetSharedGameObject(L"UI_ResultCount", GetThis<UI_ResultCount>());
		SetDrawLayer(m_Layer);

		m_toppleBuilding = (float)GetTypeStage<ResultStage>()->GetBrokenObject();

	}

	void UI_ResultCount::OnUpdate() {

		vector<VertexPositionTexture> newVertices;
		UINT num;
		int verNum = 0;
		for (UINT i = m_NumberOfDigits; i > 0; i--) {
			UINT base = (UINT)pow(10, i);
			num = ((UINT)m_TotalTime) % base;
			num = num / (base / 10);
			Vec2 uv0 = m_BackupVertices[verNum].textureCoordinate;
			uv0.x = (float)num / 10.0f;
			auto v = VertexPositionTexture(
				m_BackupVertices[verNum].position,
				uv0
			);
			newVertices.push_back(v);

			Vec2 uv1 = m_BackupVertices[verNum + 1].textureCoordinate;
			uv1.x = uv0.x + 0.1f;
			v = VertexPositionTexture(
				m_BackupVertices[verNum + 1].position,
				uv1
			);
			newVertices.push_back(v);

			Vec2 uv2 = m_BackupVertices[verNum + 2].textureCoordinate;
			uv2.x = uv0.x;

			v = VertexPositionTexture(
				m_BackupVertices[verNum + 2].position,
				uv2
			);
			newVertices.push_back(v);

			Vec2 uv3 = m_BackupVertices[verNum + 3].textureCoordinate;
			uv3.x = uv0.x + 0.1f;

			v = VertexPositionTexture(
				m_BackupVertices[verNum + 3].position,
				uv3
			);
			newVertices.push_back(v);

			verNum += 4;
		}
		auto ptrDraw = GetComponent<PTSpriteDraw>();
		ptrDraw->UpdateVertices(newVertices);


		bool start = GetTypeStage<ResultStage>()->GetCountStart();
		if (start) {
			if (m_TotalTime < m_toppleBuilding) {
				m_TotalTime++;
			}
			else if (m_TotalTime == m_toppleBuilding) {
				GetTypeStage<ResultStage>()->SetCountStart(false);
			}
		}
	}

	UI_ResultMaxCount::UI_ResultMaxCount(const shared_ptr<Stage>& StagePtr, UINT NumberOfDigits,
		const wstring& TextureKey,
		const Vec3& StartPos,
		const int& Layer) :
		GameObject(StagePtr),
		m_NumberOfDigits(NumberOfDigits),
		m_TextureKey(TextureKey),
		m_StartPos(StartPos),
		m_Layer(Layer)
	{}

	void UI_ResultMaxCount::OnCreate() {
		float xPiecesize = 1.0f / (float)m_NumberOfDigits;
		float helfSize = 0.5f;

		//インデックス配列
		vector<uint16_t> indices;
		for (UINT i = 0; i < m_NumberOfDigits; i++) {
			float vertex0 = -helfSize + xPiecesize * (float)i;
			float vertex1 = vertex0 + xPiecesize;
			//0
			m_BackupVertices.push_back(
				VertexPositionTexture(Vec3(vertex0, helfSize, 0), Vec2(0.0f, 0.0f))
			);
			//1
			m_BackupVertices.push_back(
				VertexPositionTexture(Vec3(vertex1, helfSize, 0), Vec2(0.1f, 0.0f))
			);
			//2
			m_BackupVertices.push_back(
				VertexPositionTexture(Vec3(vertex0, -helfSize, 0), Vec2(0.0f, 1.0f))
			);
			//3
			m_BackupVertices.push_back(
				VertexPositionTexture(Vec3(vertex1, -helfSize, 0), Vec2(0.1f, 1.0f))
			);
			indices.push_back(i * 4 + 0);
			indices.push_back(i * 4 + 1);
			indices.push_back(i * 4 + 2);
			indices.push_back(i * 4 + 1);
			indices.push_back(i * 4 + 3);
			indices.push_back(i * 4 + 2);
		}

		SetAlphaActive(true);
		auto ptrTrans = GetComponent<Transform>();
		ptrTrans->SetScale(Vec3(180.0f, 120.0f, 1.0f));
		ptrTrans->SetPosition(m_StartPos);
		//頂点とインデックスを指定してスプライト作成
		auto ptrDraw = AddComponent<PTSpriteDraw>(m_BackupVertices, indices);
		ptrDraw->SetTextureResource(m_TextureKey);
		GetStage()->SetSharedGameObject(L"UI_ResultMaxCount", GetThis<UI_ResultMaxCount>());
		SetDrawLayer(m_Layer);

		m_TotalTime = (float)GetTypeStage<ResultStage>()->GetMaxBrokenObject();

	}

	void UI_ResultMaxCount::OnUpdate() {

		vector<VertexPositionTexture> newVertices;
		UINT num;
		int verNum = 0;
		for (UINT i = m_NumberOfDigits; i > 0; i--) {
			UINT base = (UINT)pow(10, i);
			num = ((UINT)m_TotalTime) % base;
			num = num / (base / 10);
			Vec2 uv0 = m_BackupVertices[verNum].textureCoordinate;
			uv0.x = (float)num / 10.0f;
			auto v = VertexPositionTexture(
				m_BackupVertices[verNum].position,
				uv0
			);
			newVertices.push_back(v);

			Vec2 uv1 = m_BackupVertices[verNum + 1].textureCoordinate;
			uv1.x = uv0.x + 0.1f;
			v = VertexPositionTexture(
				m_BackupVertices[verNum + 1].position,
				uv1
			);
			newVertices.push_back(v);

			Vec2 uv2 = m_BackupVertices[verNum + 2].textureCoordinate;
			uv2.x = uv0.x;

			v = VertexPositionTexture(
				m_BackupVertices[verNum + 2].position,
				uv2
			);
			newVertices.push_back(v);

			Vec2 uv3 = m_BackupVertices[verNum + 3].textureCoordinate;
			uv3.x = uv0.x + 0.1f;

			v = VertexPositionTexture(
				m_BackupVertices[verNum + 3].position,
				uv3
			);
			newVertices.push_back(v);

			verNum += 4;
		}
		auto ptrDraw = GetComponent<PTSpriteDraw>();
		ptrDraw->UpdateVertices(newVertices);
	}



	void UI_Copy::OnCreate() {
		Draw();
		SetTime(2.0f);
	}

	void UI_Copy::OnUpdate() {
		if (m_scene && (TimeUpdate() <= 0.0f)) {
			GetStage()->AddGameObject<UI_RockBoy>(
				true,
				0
				);
			m_scene = false;
		}
	}



	UI_Time::UI_Time(const shared_ptr<Stage>& StagePtr, UINT NumberOfDigits,
		const wstring& TextureKey,
		const Vec3& StartPos,
		const float& Time,
		const int& Layer) :
		GameObject(StagePtr),
		m_NumberOfDigits(NumberOfDigits),
		m_TextureKey(TextureKey),
		m_StartPos(StartPos),
		m_TotalTime(Time),
		m_Layer(Layer)
	{}

	void UI_Time::OnCreate() {
		float xPiecesize = 1.0f / (float)m_NumberOfDigits;
		float helfSize = 0.5f;

		//インデックス配列
		vector<uint16_t> indices;
		for (UINT i = 0; i < m_NumberOfDigits; i++) {
			float vertex0 = -helfSize + xPiecesize * (float)i;
			float vertex1 = vertex0 + xPiecesize;
			//0
			m_BackupVertices.push_back(
				VertexPositionTexture(Vec3(vertex0, helfSize, 0), Vec2(0.0f, 0.0f))
			);
			//1
			m_BackupVertices.push_back(
				VertexPositionTexture(Vec3(vertex1, helfSize, 0), Vec2(0.1f, 0.0f))
			);
			//2
			m_BackupVertices.push_back(
				VertexPositionTexture(Vec3(vertex0, -helfSize, 0), Vec2(0.0f, 1.0f))
			);
			//3
			m_BackupVertices.push_back(
				VertexPositionTexture(Vec3(vertex1, -helfSize, 0), Vec2(0.1f, 1.0f))
			);
			indices.push_back(i * 4 + 0);
			indices.push_back(i * 4 + 1);
			indices.push_back(i * 4 + 2);
			indices.push_back(i * 4 + 1);
			indices.push_back(i * 4 + 3);
			indices.push_back(i * 4 + 2);
		}

		SetAlphaActive(true);
		auto ptrTrans = GetComponent<Transform>();
		ptrTrans->SetScale(Vec3(180.0f, 120.0f, 1.0f));
		ptrTrans->SetPosition(m_StartPos);
		//頂点とインデックスを指定してスプライト作成
		auto ptrDraw = AddComponent<PTSpriteDraw>(m_BackupVertices, indices);
		ptrDraw->SetTextureResource(m_TextureKey);
		GetStage()->SetSharedGameObject(L"UI_Time", GetThis<UI_Time>());
		SetDrawLayer(m_Layer);

		m_allBuilding = (float)GetTypeStage<GameStage>()->GetMaxBreakObject();

	}

	void UI_Time::OnUpdate() {

		vector<VertexPositionTexture> newVertices;
		UINT num;
		int verNum = 0;
		for (UINT i = m_NumberOfDigits; i > 0; i--) {
			UINT base = (UINT)pow(10, i);
			num = ((UINT)m_TotalTime) % base;
			num = num / (base / 10);
			Vec2 uv0 = m_BackupVertices[verNum].textureCoordinate;
			uv0.x = (float)num / 10.0f;
			auto v = VertexPositionTexture(
				m_BackupVertices[verNum].position,
				uv0
			);
			newVertices.push_back(v);

			Vec2 uv1 = m_BackupVertices[verNum + 1].textureCoordinate;
			uv1.x = uv0.x + 0.1f;
			v = VertexPositionTexture(
				m_BackupVertices[verNum + 1].position,
				uv1
			);
			newVertices.push_back(v);

			Vec2 uv2 = m_BackupVertices[verNum + 2].textureCoordinate;
			uv2.x = uv0.x;

			v = VertexPositionTexture(
				m_BackupVertices[verNum + 2].position,
				uv2
			);
			newVertices.push_back(v);

			Vec2 uv3 = m_BackupVertices[verNum + 3].textureCoordinate;
			uv3.x = uv0.x + 0.1f;

			v = VertexPositionTexture(
				m_BackupVertices[verNum + 3].position,
				uv3
			);
			newVertices.push_back(v);

			verNum += 4;
		}
		auto ptrDraw = GetComponent<PTSpriteDraw>();
		ptrDraw->UpdateVertices(newVertices);


		m_toppleBuilding = (float)GetTypeStage<GameStage>()->GetBrokenObject();
		bool start = GetTypeStage<GameStage>()->GetGameStart();
		if (start) {

			float m_raito = (m_toppleBuilding / m_allBuilding) * 100.0f;
			float elapsedTime = App::GetApp()->GetElapsedTime();
			m_TotalTime -= elapsedTime;

			if (m_raito >= 100) {
				GetTypeStage<GameStage>()->SetGameFinish(true);
				GetTypeStage<GameStage>()->SetGameStart(false);
			}
			if (m_TotalTime <= 0){
				GetTypeStage<GameStage>()->SetGameFinish(true);
				GetTypeStage<GameStage>()->SetGameStart(false);
			}
		}
	}


	UI_TimeBack::UI_TimeBack(const shared_ptr<Stage>& StagePtr, UINT NumberOfDigits,
		const wstring& TextureKey,
		const Vec3& StartPos,
		const int& Layer) :
		GameObject(StagePtr),
		m_NumberOfDigits(NumberOfDigits),
		m_TextureKey(TextureKey),
		m_StartPos(StartPos),
		m_Layer(Layer)
	{}

	void UI_TimeBack::OnCreate() {
		float xPiecesize = 1.0f / (float)m_NumberOfDigits;
		float helfSize = 1.0f;

		//インデックス配列
		vector<uint16_t> indices;
		for (UINT i = 0; i < m_NumberOfDigits; i++) {
			float vertex0 = -helfSize + xPiecesize * (float)i;
			float vertex1 = vertex0 + xPiecesize;
			//0
			m_BackupVertices.push_back(
				VertexPositionTexture(Vec3(vertex0, helfSize, 0), Vec2(0.0f, 0.0f))
			);
			//1
			m_BackupVertices.push_back(
				VertexPositionTexture(Vec3(vertex1, helfSize, 0), Vec2(0.1f, 0.0f))
			);
			//2
			m_BackupVertices.push_back(
				VertexPositionTexture(Vec3(vertex0, -helfSize, 0), Vec2(0.0f, 1.0f))
			);
			//3
			m_BackupVertices.push_back(
				VertexPositionTexture(Vec3(vertex1, -helfSize, 0), Vec2(0.1f, 1.0f))
			);
			indices.push_back(i * 4 + 0);
			indices.push_back(i * 4 + 1);
			indices.push_back(i * 4 + 2);
			indices.push_back(i * 4 + 1);
			indices.push_back(i * 4 + 3);
			indices.push_back(i * 4 + 2);
		}

		SetAlphaActive(true);
		auto ptrTrans = GetComponent<Transform>();
		ptrTrans->SetScale(Vec3(1000.0f, 1000.0f, 1.0f));
		ptrTrans->SetPosition(m_StartPos);
		//頂点とインデックスを指定してスプライト作成
		auto ptrDraw = AddComponent<PTSpriteDraw>(m_BackupVertices, indices);
		ptrDraw->SetTextureResource(m_TextureKey);
		GetStage()->SetSharedGameObject(L"UI_TimeBack", GetThis<UI_TimeBack>());
		SetDrawLayer(m_Layer);
		ptrDraw->SetDiffuse(Col4(0.2f, 0.2f, 0.9f, 0.5f));
	}

}