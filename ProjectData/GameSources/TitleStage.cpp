#include "stdafx.h"
#include "Project.h"

namespace basecross {
	void TitleStage::CreateCamera() {
		auto PtrView = CreateView<SingleView>();
		//ビューのカメラの設定
		auto PtrCamera = ObjectFactory::Create<Camera>();
		PtrView->SetCamera(PtrCamera);
		PtrCamera->SetEye(0.0f, 200.0f, -5.0f);
		PtrCamera->SetAt(0.0f, 0.0f, 0.0f);
		//マルチライトの作成
		auto PtrMultiLight = CreateLight<MultiLight>();
		//デフォルトのライティングを指定
		PtrMultiLight->SetDefaultLighting();
	}

	void TitleStage::CreateUI() {
		AddGameObject<UI_RockBoy>(
			false,
			1
			);

	}

	void TitleStage::OnCreate() {
		try {
			//ビューとライトの作成
			CreateCamera();
			CreateUI();
			AddGameObject<MapCreator>();
			AddGameObject<Ground>(
				Vec3(500.0f, 0.1f, 500.0f),
				Vec3(0.0f, 0.0f, 0.0f),
				Vec3(0.0f, -1.0f, 0.0f));

			PlayBGM(L"game_maoudamashii_5_town24.wav");
		}
		catch (...) {
			throw;
		}
	}

	void TitleStage::OnUpdate() {
		auto KeyState = App::GetApp()->GetInputDevice().GetKeyState();
		auto cntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		if (m_scene && (KeyState.m_bPressedKeyTbl[VK_SPACE] || (cntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A))) {
			AddGameObject<UI_RockBoy>(
				true,
				1
				);

			auto xAPtr = App::GetApp()->GetXAudio2Manager();
			xAPtr->Start(L"se_maoudamashii_system24.wav", 0, 0.2f);

			m_scene = false;
		}
	}
}