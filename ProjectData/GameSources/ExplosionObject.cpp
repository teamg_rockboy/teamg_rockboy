/*!
@file Player.cpp
@brief プレイヤーなど実体
*/

#include "stdafx.h"
#include "Project.h"

namespace basecross {
	ExplosionObject::ExplosionObject(const shared_ptr<Stage>& StagePtr,
		const Vec3& Position,
		const Vec3& Scale
	) :
		GameObject(StagePtr),
		m_startPos(Position),
		m_startSca(Scale)
	{}

	void ExplosionObject::OnCreate() {
		auto ptrTrans = GetComponent<Transform>();
		ptrTrans->SetScale(m_startSca);
		ptrTrans->SetPosition(m_startPos);

		auto ptrColl = AddComponent<CollisionObb>();
		ptrColl->SetAfterCollision(AfterCollision::None);

		ptrColl->SetUpdateActive(true);

		//WorldMatrixをもとにRigidbodySphereのパラメータを作成
		PsBoxParam param(ptrTrans->GetWorldMatrix(), 1.0f, false, PsMotionType::MotionTypeActive);
		auto psPtr = AddComponent<RigidbodyBox>(param);

		AddTag(L"ExpObj");
	}

	void ExplosionObject::OnUpdate() {
		DestroyObject();
	}

	void ExplosionObject::DestroyObject() {
		GetStage()->RemoveGameObject<GameObject>(GetThis<GameObject>());
	}

}
//end basecross
