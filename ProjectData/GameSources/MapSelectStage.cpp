#include "stdafx.h"
#include "Project.h"

namespace basecross {
	void MapSelectStage::CreateCamera() {
		auto ptrView = CreateView<SingleView>();
		//ビューのカメラの設定
		auto ptrMyCamera = ObjectFactory::Create<MyCamera>();
		ptrView->SetCamera(ptrMyCamera);
		ptrMyCamera->SetEye(Vec3(0.0f, 50.0f, -50.0f));
		ptrMyCamera->SetAt(Vec3(0.0f, 0.0f, 0.0f));
		//マルチライトの作成
		auto ptrMultiLight = CreateLight<MultiLight>();
		//デフォルトのライティングを指定
		ptrMultiLight->SetDefaultLighting();
	}

	void MapSelectStage::CreateUI() {
		AddGameObject<UI_RockBoy>(
			false,
			0
			);

		AddGameObject<UI_MapSelect>(
			-256.0f,
			256.0f,
			64.0f,
			-64.0f,
			Vec3(0.0f, -400.0f, 0.0f),
			Vec3(1.0f, 1.0f, 1.0f),
			Vec3(0.0f, 0.0f, 0.0f),
			20,
			Col4(1.0f, 1.0f, 1.0f, 1),
			L"MapSelect.png"
			);
		AddGameObject<UI_MapPicSelect>(
			-256.0f,
			256.0f,
			64.0f,
			-64.0f,
			Vec3(0.0f, -400.0f, 0.0f),
			Vec3(0.8f, 0.8f, 1.0f),
			Vec3(0.0f, 0.0f, 0.0f),
			0,
			Col4(1.0f, 1.0f, 1.0f, 1),
			L"Text_Stage1.png"
			);
		AddGameObject<UI_MapPicSelect>(
			-256.0f,
			256.0f,
			64.0f,
			-64.0f,
			Vec3(550.0f, -400.0f, 0.0f),
			Vec3(0.8f, 0.8f, 1.0f),
			Vec3(0.0f, 0.0f, 0.0f),
			1,
			Col4(1.0f, 1.0f, 1.0f, 1),
			L"Text_Stage2.png"
			);
		AddGameObject<UI_MapPicSelect>(
			-256.0f,
			256.0f,
			64.0f,
			-64.0f,
			Vec3(-550.0f, -400.0f, 0.0f),
			Vec3(0.8f, 0.8f, 1.0f),
			Vec3(0.0f, 0.0f, 0.0f),
			-1,
			Col4(1.0f, 1.0f, 1.0f, 1),
			L"Text_Stage3.png"
			);
		AddGameObject<UI_MapTextSelect>(
			-512.0f,
			512.0f,
			128.0f,
			-128.0f,
			Vec3(0.0f, 400.0f, 0.0f),
			Vec3(1.0f, 1.0f, 1.0f),
			Vec3(0.0f, 0.0f, 0.0f),
			20,
			Col4(1.0f, 1.0f, 1.0f, 1),
			L"Text_StageSelect.png"
			);
		AddGameObject<UI_MapTextSelect>(
			-960.0f,
			960.0f,
			150.0f,
			-150.0f,
			Vec3(0.0f, -400.0f, 0.0f),
			Vec3(1.0f, 1.0f, 1.0f),
			Vec3(0.0f, 0.0f, 0.0f),
			-5,
			Col4(0.0f, 0.0f, 0.0f, 0.5f),
			L"timeNumberBack.png"
			);
		AddGameObject<UI_MapBackSelect>(
			-960.0f,
			960.0f,
			540.0f,
			-540.0f,
			Vec3(0.0f, 0.0f, 0.0f),
			Vec3(1.0f, 1.0f, 1.0f),
			Vec3(0.0f, 0.0f, 0.0f),
			-10,
			Col4(0.5f, 0.5f, 0.5f, 1),
			L"Stage1.png"
			);
	}

	void MapSelectStage::OnCreate() {
		try
		{
			CreateCamera();
			CreateUI();
			PlayBGM(L"game_maoudamashii_5_town18.wav");
		}
		catch (...)
		{
			throw;
		}
	}

	void MapSelectStage::OnUpdate() {

	}
}
