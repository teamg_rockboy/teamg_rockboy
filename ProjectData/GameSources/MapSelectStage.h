#pragma once
#include "stdafx.h"

namespace basecross {
	class MapSelectStage : public GameSceneBase {
		int m_select = 0;
	public:
		MapSelectStage() : GameSceneBase() {}
		virtual ~MapSelectStage() {}

		virtual void OnCreate()override;
		virtual void OnUpdate()override;

		void CreateCamera();
		void CreateUI();

		void SetSelect(int select) {
			m_select = select;
		}
		int GetSelect() {
			return m_select;
		}

	};
}