#include "stdafx.h"
#include "MyPhyics.h"

namespace basecross {
	//Transformから初期化するパラメータ
	using namespace sce::PhysicsEffects;

	//--------------------------------------------------------------------------------------
	///	単体のボックスコンポーネント
	//--------------------------------------------------------------------------------------
	MyRigidbodyBox::MyRigidbodyBox(const shared_ptr<GameObject>& GameObjectPtr, const PsMyBoxParam& param) :
		RigidbodySingle(GameObjectPtr)
	{
		//空IDの取得
		auto index = GameObjectPtr->GetStage()->GetVacantPhysicsIndex();
		m_PsMyBox = GameObjectPtr->GetStage()->GetBasePhysics().AddBox(param, index);
	}

	uint16_t MyRigidbodyBox::GetIndex() const {
		return m_PsMyBox->GetIndex();
	}

	const PsMyBoxParam& MyRigidbodyBox::GetParam() const {
		return m_PsMyBox->GetParam();
	}

	OBB MyRigidbodyBox::GetOBB() const {
		OBB obb(m_PsMyBox->GetParam().m_HalfSize * 2,
			GetOrientation(), GetPosition());
		return obb;
	}

	bool MyRigidbodyBox::CollisionTestBase(const SPHERE& src) {
		bsm::Vec3 ret;
		return HitTest::SPHERE_OBB(src, GetOBB(), ret);
	}
	bool MyRigidbodyBox::CollisionTestBase(const CAPSULE& src) {
		bsm::Vec3 ret;
		return HitTest::CAPSULE_OBB(src, GetOBB(), ret);
	}
	bool MyRigidbodyBox::CollisionTestBase(const OBB& src) {
		return HitTest::OBB_OBB(src, GetOBB());
	}

	void MyRigidbodyBox::Reset(const PsMyBoxParam& param, uint16_t index) {
		m_PsMyBox = GetGameObject()->GetStage()->GetBasePhysics().AddBox(param, index);
	}

	void MyRigidbodyBox::OnDraw() {
		auto MeshRes = App::GetApp()->GetResource<MeshResource>(L"PSWIRE_PC_CUBE");
		//トランスフォームからの差分
		bsm::Mat4x4 meshtotrans;
		meshtotrans.affineTransformation(
			bsm::Vec3(0.5f),			//スケーリングは0.5f
			bsm::Vec3(0, 0, 0),		//回転の中心（重心）
			Quat(),				//回転角度
			bsm::Vec3(0, 0, 0)				//位置
		);
		DrawShapeWireFrame(MeshRes, meshtotrans);
	}

}
