#pragma once

namespace basecross {
	class Building : public ObjectBase {
		float m_gravityScale;
		Vec3 m_localGravity = Vec3(200.0f, 0.0f, 0.0f);
		bool m_topple = false;
		bool m_isAddForce = false;
		float m_addTorque = 800.0f;
		float m_toppleTimer = 0.0f;
		float m_maxToppleTime = 0.5f;
	public:
		//�\�z�Ɣj��
		Building(const shared_ptr<Stage>& StagePtr,
			const Vec3& Scale,
			const Vec3& Rotation,
			const Vec3& Position,
			const Vec3& ModelScale,
			const Vec3& ModelPosition,
			float gravityScale
		) :
			ObjectBase(StagePtr,
				Scale,
				Rotation,
				Position,
				ModelScale,
				ModelPosition
			),
			m_gravityScale(gravityScale)
		{}

		virtual ~Building();
		//������
		virtual void OnCreate() override;
		virtual void OnUpdate() override;
		virtual void OnCollisionEnter(shared_ptr<GameObject>& Other) override;
		void FallVector(Vec3 pos);
	};

	class VisibleSlop : public ObjectBase {
		int m_rotationNum = 0;
	public :
		VisibleSlop(const shared_ptr<Stage>& StagePtr,
			const Vec3& Scale,
			const Vec3& Position,
			const Vec3& Rotation,
			const Vec3& ModelScale,
			const Vec3& ModelPosition
		) :
			ObjectBase(StagePtr,
				Scale,
				Position,
				Rotation,
				ModelScale,
				ModelPosition
			)
		{}

		virtual ~VisibleSlop();
		virtual void OnCreate() override;
	};

	class Slop : public ObjectBase {
	public:
		Slop(const shared_ptr<Stage>& StagePtr,
			const Vec3& Scale,
			const Vec3& Rotation,
			const Vec3& Position,
			const Vec3& ModelScale,
			const Vec3& ModelPosition

		) :
			ObjectBase(StagePtr,
				Scale,
				Rotation,
				Position,
				ModelScale,
				ModelPosition
			)
		{}

		virtual ~Slop();
		virtual void OnCreate() override;
		virtual void OnCollisionEnter(shared_ptr<GameObject>& Other) override;
		void FallVector(Vec3 otherObjectPos);
	};


	class Bridge : public ObjectBase {
	public:
		Bridge(const shared_ptr<Stage>& StagePtr,
			const Vec3& Scale,
			const Vec3& Rotation,
			const Vec3& Position,
			const Vec3& ModelScale,
			const Vec3& ModelPosition

		) :
			ObjectBase(StagePtr,
				Scale,
				Rotation,
				Position,
				ModelScale,
				ModelPosition
			)
		{}
		virtual ~Bridge();
		virtual void OnCreate() override;
		virtual void OnCollisionEnter(shared_ptr<GameObject>& Other) override;

	};

}