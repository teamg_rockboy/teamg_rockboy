#pragma once
#include "stdafx.h"

namespace basecross {
	class Restriction : public GameObject {
		Vec3 m_Scale;
		Vec3 m_Rotation;
		Vec3 m_Position;

	public:
		Restriction(const shared_ptr<Stage>& StagePtr,
			const Vec3& Scale,
			const Vec3& Rotation,
			const Vec3& Position
		);

		virtual ~Restriction();
		virtual void OnCreate() override;
	};
}
