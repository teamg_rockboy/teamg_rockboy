#include "stdafx.h"
#include "Project.h"

namespace basecross {


	void EffectObject::OnCreate() {
		wstring DataDir;
		App::GetApp()->GetDataDirectory(DataDir);
		wstring TestEffectStr = DataDir + L"Effects\\test.efk";
		auto ShEfkInterface = GetTypeStage<GameStage>()->GetEfkInterface();
		m_efkEffect = ObjectFactory::Create<EfkEffect>(ShEfkInterface, TestEffectStr);

	}

	void EffectObject::OnUpdate() {
		auto Ptr = GetComponent<Transform>();
		auto ShEfkInterface = GetTypeStage<GameStage>()->GetEfkInterface();
		m_efkPlay = ObjectFactory::Create<EfkPlay>(m_efkEffect, Ptr->GetPosition());
	}
}
//end basecross
