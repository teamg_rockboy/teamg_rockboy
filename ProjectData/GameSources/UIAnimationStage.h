#pragma once
#include "stdafx.h"

namespace basecross {

	class UIStage : public Stage {
		void CreateCamera();

	public:
		UIStage() :Stage() {}
		virtual ~UIStage() {}

		virtual void OnCreate()override;
		virtual void OnUpdate()override;
	};

}