#pragma once

namespace basecross {
	class House : public ObjectBase {
	public:
		//�\�z�Ɣj��
		House(const shared_ptr<Stage>& StagePtr,
			const Vec3& Scale,
			const Vec3& Rotation,
			const Vec3& Position,
			const Vec3& ModelScale,
			const Vec3& ModelPosition

		) :
			ObjectBase(StagePtr,
				Scale,
				Rotation,
				Position,
				ModelScale,
				ModelPosition
			)
		{}

		virtual ~House();
		//������
		virtual void OnCreate() override;
		virtual void OnCollisionEnter(shared_ptr<GameObject>& Other) override;

	};
}