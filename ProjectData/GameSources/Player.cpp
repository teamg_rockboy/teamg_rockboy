/*!
@file Player.cpp
@brief プレイヤーなど実体
*/

#include "stdafx.h"
#include "Project.h"

namespace basecross{
	//構築と破棄
	Player::~Player() {}

	//初期化
	void Player::OnCreate() {
		//初期位置などの設定
		SetTrans();
		SetModelName(L"m_rock.bmf");
		ReadBmfData();

		auto ptr = GetComponent<Transform>();

		auto ptrColl = AddComponent<CollisionObb>();
		ptrColl->SetAfterCollision(AfterCollision::None);

		//各パフォーマンスを得る
		GetStage()->SetCollisionPerformanceActive(true);
		GetStage()->SetUpdatePerformanceActive(true);
		GetStage()->SetDrawPerformanceActive(true);

		AddTag(L"Player");
		
		//WorldMatrixをもとにRigidbodySphereのパラメータを作成
		PsSphereParam param(ptr->GetWorldMatrix(), 10.0f, false, PsMotionType::MotionTypeActive);
		auto psPtr = AddComponent<RigidbodySphere>(param);

		//カメラを得る
		auto ptrCamera = dynamic_pointer_cast<MyCamera>(OnGetDrawCamera());
		if (ptrCamera) {
			//MyCameraに注目するオブジェクト（プレイヤー）の設定
			ptrCamera->SetTargetObject(GetThis<Player>());
			ptrCamera->SetTargetToAt(Vec3(0, 0.25f, 0));
		}
	}

	Vec3 Player::GetMoveVector() const {
		Vec3 angle(0, 0, 0);
		//コントローラの取得
		auto cntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		float fThumbLY = 0.0f;
		float fThumbLX = 0.0f;
		WORD wButtons = 0;
		if (cntlVec[0].bConnected) {
			fThumbLY = cntlVec[0].fThumbLY;
			fThumbLX = cntlVec[0].fThumbLX;
			wButtons = cntlVec[0].wButtons;
		}
		//キーボードの取得(キーボード優先)
		auto KeyState = App::GetApp()->GetInputDevice().GetKeyState();
		if (KeyState.m_bPushKeyTbl['W']) {
			//前
			fThumbLY = 1.0f;
		}
		else if (KeyState.m_bPushKeyTbl['S']) {
			//後ろ
			fThumbLY = -1.0f;
		}
		if (KeyState.m_bPushKeyTbl['D']) {
			//右
			fThumbLX = 1.0f;
		}
		else if (KeyState.m_bPushKeyTbl['A']) {
			//左
			fThumbLX = -1.0f;
		}
		if (fThumbLX != 0 || fThumbLY != 0) {
			float moveLength = 0;	//動いた時のスピード
			auto ptrTransform = GetComponent<Transform>();
			auto ptrCamera = OnGetDrawCamera();
			//進行方向の向きを計算
			auto front = ptrTransform->GetPosition() - ptrCamera->GetEye();
			front.y = 0;
			front.normalize();
			//進行方向向きからの角度を算出
			float frontAngle = atan2(front.z, front.x);
			//コントローラの向き計算
			float moveX = fThumbLX;
			float moveZ = fThumbLY;
			Vec2 moveVec(moveX, moveZ);
			float moveSize = moveVec.length();
			//コントローラの向きから角度を計算
			float cntlAngle = atan2(-moveX, moveZ);
			//トータルの角度を算出
			float totalAngle = frontAngle + cntlAngle;
			//角度からベクトルを作成
			angle = Vec3(cos(totalAngle), 0, sin(totalAngle));
			//正規化する
			angle.normalize();
			//移動サイズを設定。
			angle *= moveSize;
		}
		return angle;
	}


	void Player::OnUpdate() {
		bool start = GetTypeStage<GameStage>()->GetGameStart();
		bool finish = GetTypeStage<GameStage>()->GetGameFinish();

		auto vec = GetMoveVector();
		auto ptrPs = GetComponent<RigidbodySphere>();
		m_moveFlag = false;

		if (!(start) || finish) {
			vec *= 0.0f;
		}

		if (vec.x > 0) {
			if (m_maxSpeed < m_speed.x)
			{
				m_speed.x = vec.x * m_maxSpeed;
				m_moveFlag = true;
			}
			else {
				m_speed.x += vec.x * m_accSpeed;
				m_moveFlag = true;
			}
		}
		else if (vec.x < 0) {
			if (-m_maxSpeed > m_speed.x)
			{
				m_speed.x = vec.x * m_maxSpeed;
				m_moveFlag = true;
			}
			else {
				m_speed.x += vec.x * m_accSpeed;
				m_moveFlag = true;
			}
		}
		else if (m_speed.x != 0.0f) {
			if (m_speed.x > -0.25f && m_speed.x < 0.25f)
			{
				m_speed.x = 0.0f;
				m_moveFlag = true;
			}
			else
			{
				m_speed.x = m_speed.x / 1.05f;
				m_moveFlag = true;
			}
		}

		if (vec.z > 0) {
			if (m_maxSpeed < m_speed.z)
			{
				m_speed.z = vec.z * m_maxSpeed;
				m_moveFlag = true;
			}
			else {
				m_speed.z += vec.z * m_accSpeed;
				m_moveFlag = true;
			}
		}
		else if (vec.z < 0) {
			if (-m_maxSpeed > m_speed.z)
			{
				m_speed.z = vec.z * m_maxSpeed;
				m_moveFlag = true;
			}
			else {
				m_speed.z += vec.z * m_accSpeed;
				m_moveFlag = true;
			}
		}
		else if (m_speed.z != 0.0f) {
			if (m_speed.z > -0.5f && m_speed.z < 0.5f)
			{
				m_speed.z = 0.0f;
				m_moveFlag = true;
			}
			else
			{
				m_speed.z = m_speed.z / 1.05f;
				m_moveFlag = true;
			}
		}

		m_rot.x = vec.z;
		m_rot.z = vec.x;

		//速度を設定
		ptrPs->SetLinearVelocity(m_speed);
		ptrPs->SetAngularVelocity(m_rot);
		ptrPs->ApplyForce(Vec3(0.0f, -m_gravityScale, 0.0f));

		SoundController();
	}


	void Player::OnCollisionEnter(shared_ptr<GameObject>& Other) {
		if (Other->FindTag(L"Water")) {
			m_maxSpeed = m_waterSpeed;
		}
	}

	void Player::OnCollisionExit(shared_ptr<GameObject>& Other) {
		if (Other->FindTag(L"Water")) {
			m_maxSpeed = m_defaltSpeed;
		}
	}

	void Player::PlaySE(wstring se) {
		if (!m_playSeFlag) {
			auto xAPtr = App::GetApp()->GetXAudio2Manager();
			m_se = xAPtr->Start(se, XAUDIO2_LOOP_INFINITE, 0.1f);
			m_playSeFlag = true;
		}
	}

	void Player::StopSE() {
		if (m_playSeFlag) {
			auto xAPtr = App::GetApp()->GetXAudio2Manager();
			xAPtr->Stop(m_se);
			m_playSeFlag = false;
		}
	}

	void Player::SoundController() {
		if (m_moveFlag) {
			PlaySE(L"kan_ge_dosya01.wav");
		}
		else {
			StopSE();
		}
	}
}
//end basecross

