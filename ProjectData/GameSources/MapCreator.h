﻿/*!
@file MapCreator.h
@brief マップ
*/

#pragma once
#include "stdafx.h"

namespace basecross {
	enum ObjectName {
		on_Wall = 1,
		on_House,
		on_BuildingS,
		on_Water,
		on_Restriction,
		on_GasTank,
		on_Slop,
		on_VisibleSlop,
		on_Block,
		on_WallCornerRightUp,
		on_WallCornerLeftUp,
		on_WallCornerRightDown,
		on_WallCornerLeftDown,
		on_FreeHouse,
		on_Bridge,

	};

	enum WallPosition {
		w_Up,
		w_Right,
		w_Left,
		w_Down
	};

	struct ObjectState
	{
		Vec3 Scale;
		Vec3 Roteton;
		Vec3 Position;
		Vec3 ModelScale;
		Vec3 ModelPosition;
		Vec3 WaterPosition;
	};

	class MapCreator : public GameObject {
		ObjectState m_objectState;
		Vec3 m_wallScale;
		Vec3 m_builModelPos = Vec3(0.0f,-1.0f,0.0f);
		Vec3 m_builPos = Vec3(0.0f, 7.0f, 0.0f);
		float m_slopOfset;

		vector<int> m_maxMapPos;
		vector<Vec3> m_wallRotetonVec;
		float m_cellSize = 10.0f;
		float m_height;
		float m_waterHeight = 0.0f;

		vector<int> m_mapData;
		vector<Vec3> m_positionData;
		//vector<int> m_objectNameVec;

		unique_ptr<XmlDocReader> m_XmlDocReader;
		initializer_list<shared_ptr<GameObject>> m_objectTile;

		wstring m_debugText;

	public:
		MapCreator(const shared_ptr<Stage>& StagePtr);

		virtual ~MapCreator();
		virtual void OnCreate() override;

		void ReadXML();

		void GetBinaryMapData();


		void ResetObjectState();
		void CreateMap();
		void CreateObject();
		void CreateTile(int itr);
		void ChooseWall(int num);
		void CreateWall(int num);
		void AddMaxBreakObjectCount();
		void CreateHouse();

	};

}