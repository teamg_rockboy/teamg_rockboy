
/*!
@file Scene.cpp
@brief シーン実体
*/

#include "stdafx.h"
#include "Project.h"

namespace basecross{
	//--------------------------------------------------------------------------------------
	///	ゲームシーン
	//--------------------------------------------------------------------------------------
	void Scene::CreateResourses() {
		wstring dataDir;
		App::GetApp()->GetAssetsDirectory(dataDir);

		wstring mediaDir;
		App::GetApp()->GetDataDirectory(mediaDir);

		FindFile(dataDir);
		FindFile(mediaDir + L"Sound/SE/");
		FindFile(mediaDir + L"Sound/BGM/");
		FindFile(mediaDir + L"Texters/");
		FindMediaDirectoryFile(mediaDir + L"Model/");

	}

	void Scene::FindMediaDirectoryFile(wstring dir) {
		wstring folderDir = dir + L"Wall/";

		//メッシュを登録
		auto wallMesh = MeshResource::CreateStaticModelMesh(folderDir, L"m_wall.bmf");
		App::GetApp()->RegisterResource(L"m_wall.bmf", wallMesh);

		folderDir = dir + L"Rock/";
		auto rockMesh = MeshResource::CreateStaticModelMesh(folderDir, L"m_rock.bmf");
		App::GetApp()->RegisterResource(L"m_rock.bmf", rockMesh);

		folderDir = dir + L"GasTank/";
		auto gasTankMesh = MeshResource::CreateStaticModelMesh(folderDir, L"m_GasTank.bmf");
		App::GetApp()->RegisterResource(L"m_GasTank.bmf", gasTankMesh);

		folderDir = dir + L"Buil_S/";
		auto buil_sMesh = MeshResource::CreateStaticModelMesh(folderDir, L"m_Buil_s.bmf");
		App::GetApp()->RegisterResource(L"m_Buil_s.bmf", buil_sMesh);

		folderDir = dir + L"Buil_M/";
		auto buil_mMesh = MeshResource::CreateStaticModelMesh(folderDir, L"m_buil.bmf");
		App::GetApp()->RegisterResource(L"m_buil.bmf", buil_mMesh);

		folderDir = dir + L"House/";
		auto houseMesh = MeshResource::CreateStaticModelMesh(folderDir, L"m_House.bmf");
		App::GetApp()->RegisterResource(L"m_House.bmf", houseMesh);

		//folderDir = dir + L"House_Blue/";
		//auto house_blueMesh = MeshResource::CreateStaticModelMesh(folderDir, L"m_House.bmf");
		//App::GetApp()->RegisterResource(L"m_House.bmf", house_blueMesh);

		folderDir = dir + L"Wall_Corner/";
		auto wall_cornerMesh = MeshResource::CreateStaticModelMesh(folderDir, L"m_wall_corner.bmf");
		App::GetApp()->RegisterResource(L"m_wall_corner.bmf", wall_cornerMesh);


	}

	void Scene::FindFile(wstring dir) {
		HANDLE hFind;
		WIN32_FIND_DATA win32fd;

		wstring newdir = dir + L"*.*";
		const wchar_t *dirExtension = newdir.c_str();

		hFind = FindFirstFile(dirExtension, &win32fd);

		do {
			// 属性がFILE_ATTRIBUTE_DIRECTORYなら
			if (win32fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
				// ディレクトリ名を取得
				wstring ditectoryName = win32fd.cFileName;
				// 新しいフォルダの場所
				wstring newDateDir = dir + ditectoryName + L"/";
				if (ditectoryName.find(L".")) {
					// その中を検索
					FindFile(newDateDir);
				}
			}
			else {
				wstring fileName = win32fd.cFileName;

				auto exe = fileName.substr(fileName.find(L"."), fileName.length());

				//画像ファイルだった場合
				if (exe == L".png" || exe == L".tga" || exe == L".jpg") {
					// ファイルの場所
					wstring strTexture = dir + fileName;
					// テクスチャーを登録
					App::GetApp()->RegisterTexture(fileName, strTexture);
				}

				if (exe == L".wav") {
					wstring OpenWav = dir + fileName;

					App::GetApp()->RegisterWav(fileName, OpenWav);
				}
			}
		} while (FindNextFile(hFind, &win32fd));

		// 後処理
		FindClose(hFind);

	}

	void Scene::OnCreate() {
		try {
			//リソース作成
			CreateResourses();
			ResetStageName();
			SetStageState(StageState::copyright);
		}
		catch (...) {
			throw;
		}
	}

	Scene::Scene() {}

	Scene::~Scene() {
	}

	void Scene::OnEvent(const shared_ptr<Event>& event) {

		switch (m_stageState)
		{
		case StageState::copyright:
			ResetActiveStage<CopywriterStage>();
			break;
		case StageState::title:
			ResetActiveStage<TitleStage>();
			break;
		case StageState::game1:
			ResetActiveStage<GameStage>();
			break;
		case StageState::debug:
			ResetActiveStage<GameStage>();
			break;
		case StageState::result:
			ResetActiveStage<ResultStage>();
			break;
		case StageState::mapselect:
			ResetActiveStage<MapSelectStage>();
			break;
		case StageState::gameover:
			ResetActiveStage<GameOverStage>();
			break;
		case StageState::ui:
			ResetActiveStage<UIStage>();
			break;
		default:
			break;
		}
	}


	void Scene::ResetStageName() {
		m_stageName = {
			L"CopyrightStage",
			L"TitleStage",
			L"Stage1",
			L"Debug",
			L"ui",
			L"GameResultStage",
			L"MapSelectStage"
		};

		m_stageNameKey = {
			L"CopyrightStage",
			L"TitleStage",
			L"Stage1",
			L"Debug",
			L"ui",
			L"GameResultStage",
			L"MapSelectStage"
		};

		m_gameStageName = {
			L"Stage1",
			L"Stage2",
			L"Stage3",
			L"Stage4",
			L"Stage5",
			L"Stage6",
			L"Stage7",
			L"Stage8",
			L"Stage9",
			L"Stage10"
		};
	}
}
//end basecross
