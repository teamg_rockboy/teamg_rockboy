#include "stdafx.h"
#include "Project.h"

namespace basecross {
	void ResultStage::CreateCamera() {
		auto PtrView = CreateView<SingleView>();
		//ビューのカメラの設定
		auto PtrCamera = ObjectFactory::Create<Camera>();
		PtrView->SetCamera(PtrCamera);
		PtrCamera->SetEye(0.0f, 0.0f, -10.0f);
		PtrCamera->SetAt(0.0f, 0.0f, 0.0f);
		//マルチライトの作成
		auto PtrMultiLight = CreateLight<MultiLight>();
		//デフォルトのライティングを指定
		PtrMultiLight->SetDefaultLighting();
	}

	void ResultStage::CreateUI() {
		AddGameObject<UI_RockBoy>(
			false,
			2
			);

		AddGameObject<UI_Result>(
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			Vec3(0.0f, 0.0f, 0.0f),
			Vec3(1.0f, 1.0f, 1.0f),
			Vec3(0.0f, 0.0f, 0.0f),
			0,
			Col4(1.0f, 1.0f, 1.0f, 1),
			L"Text_GoTitle.png"
			);

		AddGameObject<UI_ResultLogo>(
			-200.0f,
			200.0f,
			50.0f,
			-50.0f,
			Vec3(-250.0f,-300.0f,0.0f),
			Vec3(1.0f,1.0f,1.0f),
			Vec3(0.0f,0.0f,0.0f),
			20,
			Col4(1.0f, 1.0f, 1.0f, 1),
			L"Text_GoTitle.png"
			);
		AddGameObject<UI_ResultLogo>(
			-200.0f,
			200.0f,
			50.0f,
			-50.0f,
			Vec3(250.0f,-300.0f,0.0f),
			Vec3(1.0f,1.0f,1.0f),
			Vec3(0.0f,0.0f,0.0f),
			20,
			Col4(1.0f, 1.0f, 1.0f, 1),
			L"Text_Onemore.png"
			);
		AddGameObject<UI_ResultSelect>(
			-180.0f,
			180.0f,
			50.0f,
			-50.0f,
			Vec3(-250.0f,-150.0f,0.0f),
			Vec3(1.0f,1.0f,1.0f),
			Vec3(0.0f,0.0f,0.0f),
			1,
			Col4(0.0f, 1.0f, 1.0f, 1),
			L"timeNumberBack.png"
			);
		AddGameObject<UI_ResultLogo>(
			-500.0f,
			500.0f,
			100.0f,
			-100.0f,
			Vec3(0.0f, -100.0f, 0.0f),
			Vec3(1.0f, 1.0f, 1.0f),
			Vec3(0.0f, 0.0f, 0.0f),
			10,
			Col4(1.0f, 1.0f, 1.0f, 1),
			L"UI_houkai.png"
			);
		AddGameObject<UI_ResultGageBar>(
			-482.0f,
			482.0f,
			100.0f,
			-100.0f,
			Vec3(0.0f, -100.0f, 0.0f),
			Vec3(1.0f, 1.0f, 1.0f),
			Vec3(0.0f, 0.0f, 0.0f),
			11,
			Col4(0.5f, 0.5f, 0.5f, 1),
			L"UI_houkaikara.png"
			);
		AddGameObject<UI_ResultLogo>(
			-500.0f,
			500.0f,
			100.0f,
			-100.0f,
			Vec3(0.0f, -100.0f, 0.0f),
			Vec3(1.0f, 1.0f, 1.0f),
			Vec3(0.0f, 0.0f, 0.0f),
			12,
			Col4(1.0f, 1.0f, 1.0f, 1),
			L"UI_memori.png"
			);
		if (m_gameCrearFlag) {

			AddGameObject<UI_ResultLogo>(
				-400.0f,
				400.0f,
				80.0f,
				-80.0f,
				Vec3(0.0f, 250.0f, 0.0f),
				Vec3(2.0f, 2.0f, 1.0f),
				Vec3(0.0f, 0.0f, 0.0f),
				12,
				Col4(0.5f, 0.5f, 0.5f, 1),
				L"Text_GameClear.png"
				);
		}
		else {
			AddGameObject<UI_ResultLogo>(
				-400.0f,
				400.0f,
				80.0f,
				-80.0f,
				Vec3(0.0f, 250.0f, 0.0f),
				Vec3(2.0f, 2.0f, 1.0f),
				Vec3(0.0f, 0.0f, 0.0f),
				12,
				Col4(0.5f, 0.5f, 0.5f, 1),
				L"Text_GameOver.png"
				);
		}

		AddGameObject<UI_GameStage>(
			-1000.0f,
			1000.0f,
			550.0f,
			-550.0f,
			Vec3(0.0f, 0.0f, 0.0f),
			Vec3(1.0f, 1.0f, 1.0f),
			Vec3(0.0f, 0.0f, 0.0f),
			-10,
			Col4(1.0f, 1.0f, 1.0f, 1),
			L"sky_texture.jpg"
			);

	}

	void ResultStage::OnCreate() {
		try {
			//ビューとライトの作成
			ReadGameData();

			CreateCamera();
			CreateUI();
			SelectBGM();
			PlayBGM(m_bgmName);
			AddGameObject<MapCreator>();
			AddGameObject<Ground>(
				Vec3(500.0f, 0.1f, 500.0f),
				Vec3(0.0f, 0.0f, 0.0f),
				Vec3(0.0f, -1.0f, 0.0f));

		}
		catch (...) {
			throw;
		}
	}

	void ResultStage::OnUpdate() {

	}

	void ResultStage::ReadGameData() {
		wstring dataDir;

		App::GetApp()->GetDataDirectory(dataDir);
		unique_ptr<XmlDocReader> xmlDocReader = nullptr;

		xmlDocReader.reset(new XmlDocReader(dataDir + L"XML/" + L"GameData.xml"));

		auto gameDataNode = xmlDocReader->GetSelectSingleNode(L"GameData/BrokenObject");
		wstring SetGameDataStr = XmlDocReader::GetText(gameDataNode);
		m_brokenObejcNum = (int)stoi(SetGameDataStr);

		gameDataNode = xmlDocReader->GetSelectSingleNode(L"GameData/MaxBrakeObject");
		SetGameDataStr = XmlDocReader::GetText(gameDataNode);
		m_maxBrakeObject = (int)stoi(SetGameDataStr);

		gameDataNode = xmlDocReader->GetSelectSingleNode(L"GameData/ClearFlag");
		SetGameDataStr = XmlDocReader::GetText(gameDataNode);

		if (SetGameDataStr == L"true") {
			m_gameCrearFlag = true;
		}

	}

	void ResultStage::SelectBGM() {
		if (m_gameCrearFlag) {
			m_bgmName = L"game_maoudamashii_8_piano10.wav";
		}
		else {
			m_bgmName = L"game_maoudamashii_8_piano09.wav";
		}
	}
}