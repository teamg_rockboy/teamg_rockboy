/*!
@file MapCreator.h
@brief �}�b�v
*/

#pragma once
#include "stdafx.h"
#include "EfkInterface.h"

namespace basecross {
	class EffectObject : public GameObject {
		shared_ptr<EfkEffect> m_efkEffect;

		shared_ptr<EfkPlay> m_efkPlay;

	public:
		EffectObject(const shared_ptr<Stage>& StagePtr,
			const Vec3& Scale,
			const Vec3& Rotation,
			const Vec3& Position,
			const wstring& effectName
		);

		virtual void OnCreate() override;
		virtual void OnUpdate() override;
	};
}
//end basecross

