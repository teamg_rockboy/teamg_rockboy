/*!
@file MapCreator.h
@brief �}�b�v
*/

#pragma once
#include "stdafx.h"

namespace basecross {
	class GasTank : public ObjectBase {
	public:
		GasTank(const shared_ptr<Stage>& StagePtr,
			const Vec3& Scale,
			const Vec3& Rotation,
			const Vec3& Position,
			const Vec3& ModelScale,
			const Vec3& ModelPosition

		):
			ObjectBase(StagePtr,
				Scale,
				Rotation,
				Position,
				ModelScale,
				ModelPosition
			)
		{}

		virtual ~GasTank() {};

		virtual void OnCreate() override;

		virtual void OnCollisionEnter(shared_ptr<GameObject>& Other) override;

	};
}
//end basecross
