/*!
@file ObjectBase.cpp
@brief オブジェクトのもととなる
*/

#include "stdafx.h"
#include "Project.h"


namespace basecross {
	ObjectBase::ObjectBase(const shared_ptr<Stage>& StagePtr,
		const Vec3& Scale,
		const Vec3& Rotation,
		const Vec3& Position,
		const Vec3& ModelScale,
		const Vec3& ModelPosition
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position),
		m_ModelScale(ModelScale),
		m_ModelPosition(ModelPosition)
	{}

	//--------------------------------------------------------------------------------------
	/// .bmfファイルの名前を入れるセッター
	//--------------------------------------------------------------------------------------
	void ObjectBase::SetModelName(wstring fileName) {
		m_modelName = fileName;
	}

	//--------------------------------------------------------------------------------------
	/// .efkファイルの名前を入れるセッター
	//--------------------------------------------------------------------------------------
	void ObjectBase::SetEffectNum(int efk) {
		m_effectNum = efk;
	}

	//--------------------------------------------------------------------------------------
	/// 3Dモデルと影を適用する
	//--------------------------------------------------------------------------------------
	void ObjectBase::ReadBmfData() {

		//スケール
		//オリジナルローテーション
		//ローテーション
		//ポジション
		m_spanMat.affineTransformation(
			m_ModelScale,
			Vec3(0.0f, 0.0f, 0.0f),
			Vec3(0.0f, 0.0f, 0.0f),
			m_ModelPosition
		);

		//影をつける
		auto ptrShadow = AddComponent<Shadowmap>();
		ptrShadow->SetMeshResource(m_modelName);
		ptrShadow->SetMeshToTransformMatrix(m_spanMat);

		//3Dデータを設定する
		auto ptrDraw = AddComponent<PNTStaticModelDraw>();
		ptrDraw->SetMeshResource(m_modelName);
		ptrDraw->SetMeshToTransformMatrix(m_spanMat);
		ptrDraw->SetDrawActive(true);

		//ptrDraw->SetEmissive(m_objectColor);
		m_ptrDraw = GetComponent<PNTStaticModelDraw>();

	}

	//--------------------------------------------------------------------------------------
	///	トランスフォームの初期化
	//--------------------------------------------------------------------------------------
	void ObjectBase::SetTrans() {
		auto ptrTransform = GetComponent<Transform>();
		ptrTransform->SetScale(m_Scale);
		ptrTransform->SetRotation(m_Rotation);
		ptrTransform->SetPosition(m_Position);
	}
	
	//--------------------------------------------------------------------------------------
	///	エフェクトの大きさを初期化
	//--------------------------------------------------------------------------------------
	void ObjectBase::SetEffectScale(Vec3 scale) {
		m_effectScale = scale;
	}

	//--------------------------------------------------------------------------------------
	///	オブジェクトの色を初期化
	//--------------------------------------------------------------------------------------
	void ObjectBase::SetObjectColor(Col4 scale) {
		m_objectColor = scale;
		m_ptrDraw->SetDiffuse(m_objectColor);

	}


	//--------------------------------------------------------------------------------------
	///	オブジェクトをデストロイする
	//--------------------------------------------------------------------------------------
	void ObjectBase::MyDesObject() {
		PlayEffect();

		auto xAPtr = App::GetApp()->GetXAudio2Manager();
		xAPtr->Start(L"se_maoudamashii_explosion05.wav", 0, 0.1f);

		GetStage()->RemoveGameObject<GameObject>(GetThis<GameObject>());
	}

	//--------------------------------------------------------------------------------------
	/// エフェクトの再生
	//--------------------------------------------------------------------------------------
	void ObjectBase::PlayEffect() {
		auto ptrPos = GetComponent<Transform>()->GetPosition();
		auto efk = GetTypeStage<GameStage>()->GetEfkEffect();
		m_efkPlay = ObjectFactory::Create<EfkPlay>(efk[m_effectNum], ptrPos);
		m_efkPlay->SetScale(m_effectScale);
		GetTypeStage<GameStage>()->AddEfkPlay(m_efkPlay);
		GetTypeStage<GameStage>()->AddBrokenObject(1);
	}

	void ObjectBase::VisibleBuilding() {
		auto ptrTrans = GetComponent<Transform>();
		//物理計算ボックス
		PsMyBoxParam param(ptrTrans->GetWorldMatrix(), 500.0f, true, PsMotionType::MotionTypeFixed);
		auto PsPtr = AddComponent<MyRigidbodyBox>(param);
		SetDrawActive(true);
	}

	void ObjectBase::FlashingObject() {
		float flashColor = 0.5f + sin(3.14f * 2.0f / 1.0f * m_flashingTime) * 0.5f;
		m_objectColor = Col4(flashColor, flashColor, flashColor, 1.0f);
		m_ptrDraw->SetDiffuse(m_objectColor);
		m_flashingTime += App::GetApp()->GetElapsedTime();
	}

	//構築と破棄
	Ground::Ground(const shared_ptr<Stage>& StagePtr,
		const Vec3& Scale,
		const Vec3& Rotation,
		const Vec3& Position
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)
	{}

	Ground::~Ground() {}

	//初期化
	void Ground::OnCreate() {
		auto ptrTrans = GetComponent<Transform>();

		ptrTrans->SetScale(m_Scale);
		ptrTrans->SetPosition(m_Position);

		//タグをつける
		AddTag(L"Ground");

		//影をつける
		auto ptrShadow = AddComponent<Shadowmap>();
		ptrShadow->SetMeshResource(L"DEFAULT_CUBE");

		auto ptrDraw = AddComponent<BcPNTStaticDraw>();
		ptrDraw->SetFogEnabled(false);
		ptrDraw->SetMeshResource(L"DEFAULT_CUBE");
		ptrDraw->SetOwnShadowActive(true);
		ptrDraw->SetTextureResource(L"Tx_Asphalt.png");
		//物理計算ボックス
		PsBoxParam param(ptrTrans->GetWorldMatrix(), 0.0f, true, PsMotionType::MotionTypeFixed);
		auto PsPtr = AddComponent<RigidbodyBox>(param);

	}

	Wall::~Wall() {}

	//初期化
	void Wall::OnCreate() {
		SetTrans();

		auto ptrTransform = GetComponent<Transform>();

		//物理計算ボックス
		PsBoxParam param(ptrTransform->GetWorldMatrix(), 0.0f, true, PsMotionType::MotionTypeFixed);
		auto PsPtr = AddComponent<RigidbodyBox>(param);

		//タグをつける
		AddTag(L"Wall");
		SetModelName(L"m_wall.bmf");
		ReadBmfData();
		SetObjectColor(Col4(1.0f, 1.0f, 1.0f, 1.0f));

	}
}
