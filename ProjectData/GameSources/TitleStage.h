#pragma once
#include "stdafx.h"

namespace basecross {

	class TitleStage : public GameSceneBase {
		void CreateCamera();
		void CreateUI();
		bool m_scene = true;
	public:
		TitleStage() :GameSceneBase() {}
		virtual ~TitleStage() {}

		virtual void OnCreate()override;
		virtual void OnUpdate()override;
	};

}