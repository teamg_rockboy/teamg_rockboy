
#include "stdafx.h"
#include "Project.h"


namespace basecross {
	//構築と破棄
	Building::~Building() {}

	//初期化
	void Building::OnCreate() {
		auto ptrTrans = GetComponent<Transform>();
		SetTrans();
		SetModelName(L"m_Buil_s.bmf");
		ReadBmfData();
		SetEffectNum(efk_Hakai);
		SetEffectScale(Vec3(1.0f, 1.0f, 1.0f));
		SetObjectColor(Col4(1.0f, 1.0f, 1.0f, 1.0f));

		auto ptrCol = AddComponent<CollisionObb>();
		ptrCol->SetMakedSize(Vec3(1.0f,1.5f,1.0f));
		ptrCol->SetAfterCollision(AfterCollision::None);
		AddTag(L"Building");

		//物理計算ボックス
		PsMyBoxParam param(ptrTrans->GetWorldMatrix(), 1000.0, true, PsMotionType::MotionTypeActive);
		auto ptrPs = AddComponent<MyRigidbodyBox>(param);

	}

	void Building::OnCollisionEnter(shared_ptr<GameObject>& Other) {
		bool isHitObject = Other->FindTag(L"Player") || Other->FindTag(L"Building") || Other->FindTag(L"ExpObj");
		if (isHitObject && !m_topple) {
			GetTypeStage<GameStage>()->AddFallBuilObject(GetThis<Building>());
			FallVector(Other->GetComponent<Transform>()->GetPosition());
		}		
	}

	void Building::FallVector(Vec3 pos)
	{
		Vec3 m_pos = GetComponent<Transform>()->GetPosition();
		float x = m_pos.x - pos.x;
		float z = m_pos.z - pos.z;

		if (x * x > z * z)
		{
			if (x > 0)
			{
				m_localGravity = Vec3(0, 0, -1);
			}
			else
			{
				m_localGravity = Vec3(0, 0, 1);
			}
		}
		else
		{
			if (z > 0)
			{
				m_localGravity = Vec3(1, 0, 0);
			}
			else
			{
				m_localGravity = Vec3(-1, 0, 0);
			}
		}
		m_topple = true;
		m_isAddForce = true;
	}

	void Building::OnUpdate()
	{
		if (m_isAddForce) {

			float time = App::GetApp()->GetElapsedTime();
			m_toppleTimer += time;

			auto ptrPs = GetComponent<MyRigidbodyBox>();

			ptrPs->ApplyTorque(m_localGravity * (m_addTorque, 0.0f, m_addTorque));

			if (m_toppleTimer >= m_maxToppleTime)
			{
				m_isAddForce = false;
				ptrPs->ApplyForce(Vec3(0.0f));
			}
		}

		if(m_topple){
			auto ptrPs = GetComponent<MyRigidbodyBox>();
			ptrPs->ApplyForce(Vec3(0.0f, -m_gravityScale, 0.0f));
		}
	}


	Slop::~Slop() {}

	void Slop::OnCreate()
	{
		auto ptrTrans = GetComponent<Transform>();

		SetTrans();
		SetModelName(L"m_Buil_s.bmf");
		ReadBmfData();
		SetObjectColor(Col4(0.0f, 0.0f, 0.0f, 1.0f));

		auto ptrCol = AddComponent<CollisionObb>();
		ptrCol->SetAfterCollision(AfterCollision::None);
		SetDrawActive(false);

		AddTag(L"Slop");

	}

	void Slop::OnCollisionEnter(shared_ptr<GameObject>& Other)
	{
		if (Other->FindTag(L"Building"))
		{
			FallVector(Other->GetComponent<Transform>()->GetPosition());
			VisibleBuilding();
		}
	}

	void Slop::FallVector(Vec3 otherObjectPos)
	{
		Vec3 m_pos = GetComponent<Transform>()->GetPosition();
		Vec3 rot = GetComponent<Transform>()->GetRotation();
		float x = m_pos.x - otherObjectPos.x;
		float z = m_pos.z - otherObjectPos.z;

		if (x * x > z * z)
		{
			if (x > 0)
			{
				rot = Vec3(0.0f, 0.0f, -0.8f);
			}
			else
			{
				rot = Vec3(0.0f, 0.0f, 0.8f);
			}
		}
		else
		{
			if (z > 0)
			{
				rot = Vec3(0.8f, 0.0f, 0.0f);
			}
			else
			{
				rot = Vec3(-0.8f, 0.0f, 0.0f);
			}
		}
		GetComponent<Transform>()->SetRotation(rot);
	}

	VisibleSlop::~VisibleSlop() {}

	void VisibleSlop::OnCreate()
	{
		auto ptrTrans = GetComponent<Transform>();

		SetTrans();
		SetModelName(L"m_Buil_s.bmf");
		ReadBmfData();
		SetObjectColor(Col4(0.0f, 0.0f, 0.0f, 1.0f));

		auto ptrCol = AddComponent<CollisionObb>();
		ptrCol->SetAfterCollision(AfterCollision::None);

		//物理計算ボックス
		PsMyBoxParam param(ptrTrans->GetWorldMatrix(), 1000.0, true, PsMotionType::MotionTypeFixed);
		auto ptrPs = AddComponent<MyRigidbodyBox>(param);

		AddTag(L"VisibleSlop");

	}


	Bridge::~Bridge() {}

	void Bridge::OnCreate()
	{
		auto ptrTrans = GetComponent<Transform>();
		SetTrans();
		SetModelName(L"m_Buil_s.bmf");
		ReadBmfData();

		AddTag(L"Bridge");
		Vec3& Size = Vec3(1.0f, 10.0f, 1.0f);
		SetDrawActive(false);
		VisibleBuilding();
		SetObjectColor(Col4(0.0f, 0.0f, 0.0f, 1.0f));
	}

	void Bridge::OnCollisionEnter(shared_ptr<GameObject>& Other)
	{
		if (Other->FindTag(L"Building"))
		{
			VisibleBuilding();
		}
	}


}