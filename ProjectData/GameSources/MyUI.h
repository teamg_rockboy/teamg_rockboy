#pragma once
#include "stdafx.h"

namespace basecross {


	class UI_Animation :public GameObject {
		float m_Vertex_L;
		float m_Vertex_R;
		float m_Vertex_U;
		float m_Vertex_D;

		float m_firstVertex[4];
		float m_Vertex_LenghtX;
		float m_Vertex_LenghtY;

		int m_Vertex;

		float m_fullSize;
		float m_size;
		float m_colSize;

		Vec3 m_pos;
		Vec3 m_firstPos;
		float m_speedPos;
		Vec3 m_movePos;
		Vec3 m_finishPos;
		bool m_loopPos;

		Vec3 m_scale;
		Vec3 m_firstScale;
		float m_speedScale;
		Vec3 m_moveScale;
		Vec3 m_finishScale;
		bool m_loopScale;

		Vec3 m_rot;
		Vec3 m_firstRot;
		float m_speedRot;
		Vec3 m_moveRot;
		Vec3 m_finishRot;
		bool m_loopRot;
		bool m_infRot;

		Col4 m_color;
		Col4 m_firstColor;
		Col4 m_speedColor;
		Col4 m_finishColor;
		Col4 m_moveColor;
		bool m_loopColor;

		int m_Layer;

		wstring m_Textures;
		
		float m_firstTime;
		float m_time;
		

	public:
		shared_ptr<SoundItem> m_se_UI;

		UI_Animation(const shared_ptr<Stage>& StagePtr,
			const float& L,
			const float& R,
			const float& U,
			const float& D,
			const Vec3& pos,
			const Vec3& scale,
			const Vec3& rot,
			const int& Layer,
			const Col4& color,
			const wstring& Textures
		);
		virtual ~UI_Animation();
		virtual void Draw();
		virtual void VerteicesCreate();
		virtual void TransformCreate();
		virtual void TransformResetCreate();
		virtual void VerteicesUpdate(float size);
		virtual void VerteicesSetUpdate();
		virtual void PositionUpdate();
		virtual void ScaleUpdate();
		virtual void RotationUpdate();
		virtual void ColorUpdate();
		virtual float TimeUpdate();
		void SetVertex(int vertex, float fullSize, float colSize) {
			m_Vertex = vertex;
			m_fullSize = fullSize;
			m_colSize = colSize;
		}
		void SetPosition(float speedPos, Vec3 finishPos, bool loopPos) {
			m_speedPos = speedPos;
			m_finishPos = finishPos;
			m_loopPos = loopPos;
		}
		void SetRotation(float speedRot, Vec3 finishRot, bool loopRot,bool infRot) {
			m_speedRot = speedRot;
			m_finishRot = finishRot;
			m_loopRot = loopRot;
			m_infRot = infRot;
		}
		void SetScale(float speedScale, Vec3 finishScale, bool loopScale) {
			m_speedScale = speedScale;
			m_finishScale = finishScale;
			m_loopScale = loopScale;
		}
		void SetColor(Col4 speedColor, Col4 finishColor, bool loopColor) {
			m_speedColor = speedColor;
			m_finishColor = finishColor;
			m_loopColor = loopColor;
		}
		void SetTime(float time) {
			m_time = time;
			m_firstTime = time;
		}
		float GetFirstTime() {
			return m_firstTime;
		}
		Col4 GetColor() {
			return m_firstColor;
		}
		int GetLayer() {
			return m_Layer;
		}
	};



	class UI_RockBoy : public GameObject {
		bool m_switch;
		int m_scene;
	public:
		UI_RockBoy(const shared_ptr<Stage>& StagePtr, const bool& rock, const int& scene) :
			GameObject(StagePtr),m_switch(rock),m_scene(scene){}
		virtual ~UI_RockBoy() {};
		virtual void OnCreate() override;

		int GetScene() {
			return m_scene;
		}
	};

	class UI_RockBoyOrigin : public UI_Animation {
		bool m_switch;
		int m_scene;
	public:
		UI_RockBoyOrigin(const shared_ptr<Stage>& StagePtr,
			const float& L,
			const float& R,
			const float& U,
			const float& D,
			const Vec3& pos,
			const Vec3& scale,
			const Vec3& rot,
			const int& Layer,
			const Col4& color,
			const wstring& Textures,
			const bool& rock,
			const int& scene
		) :
			UI_Animation(
				StagePtr,
				L,
				R,
				U,
				D,
				pos,
				scale,
				rot,
				Layer,
				color,
				Textures
			),
			m_switch(rock),
			m_scene(scene)
		{}
		virtual ~UI_RockBoyOrigin() {};
		virtual void OnCreate() override;
		virtual void OnUpdate() override;
		void BeginCreate();
		void EndCreate();
		void BeginUpdate();
		void EndUpdate();
	};




	class UI_GameStage : public UI_Animation {
	public :
		UI_GameStage(const shared_ptr<Stage>& StagePtr,
			const float& L,
			const float& R,
			const float& U,
			const float& D,
			const Vec3& pos,
			const Vec3& scale,
			const Vec3& rot,
			const int& Layer,
			const Col4& color,
			const wstring& Textures
		) :
			UI_Animation(
				StagePtr,
				L,
				R,
				U,
				D,
				pos,
				scale,
				rot,
				Layer,
				color,
				Textures
			)
		{}
		virtual ~UI_GameStage() {};
		virtual void OnCreate() override;
	};

	class GageBar : public UI_Animation {
	public :
		GageBar(const shared_ptr<Stage>& StagePtr,
			const float& L,
			const float& R,
			const float& U,
			const float& D,
			const Vec3& pos,
			const Vec3& scale,
			const Vec3& rot,
			const int& Layer,
			const Col4& color,
			const wstring& Textures
		) :
			UI_Animation(
				StagePtr,
				L,
				R,
				U,
				D,
				pos,
				scale,
				rot,
				Layer,
				color,
				Textures
			)
		{}
		virtual ~GageBar() {};
		virtual void OnCreate() override;
		virtual void OnUpdate() override;
	};

	

	class UI_GameReady : public UI_Animation {
		int m_draw = 0;
	public:
		UI_GameReady(const shared_ptr<Stage>& StagePtr,
			const float& L,
			const float& R,
			const float& U,
			const float& D,
			const Vec3& pos,
			const Vec3& scale,
			const Vec3& rot,
			const int& Layer,
			const Col4& color,
			const wstring& Textures
		) :
			UI_Animation(
				StagePtr,
				L,
				R,
				U,
				D,
				pos,
				scale,
				rot,
				Layer,
				color,
				Textures
			)
		{}
		virtual ~UI_GameReady() {};
		virtual void OnCreate() override;
		virtual void OnUpdate() override;
	};

	class UI_GameStart : public UI_Animation {
	public :
		UI_GameStart(const shared_ptr<Stage>& StagePtr,
			const float& L,
			const float& R,
			const float& U,
			const float& D,
			const Vec3& pos,
			const Vec3& scale,
			const Vec3& rot,
			const int& Layer,
			const Col4& color,
			const wstring& Textures
		) :
			UI_Animation(
				StagePtr,
				L,
				R,
				U,
				D,
				pos,
				scale,
				rot,
				Layer,
				color,
				Textures
			)
		{}
		virtual ~UI_GameStart() {};
		virtual void OnCreate() override;
		virtual void OnUpdate() override;
	};

	class UI_GameFinish : public UI_Animation {
		int m_draw = 0;
	public:
		UI_GameFinish(const shared_ptr<Stage>& StagePtr,
			const float& L,
			const float& R,
			const float& U,
			const float& D,
			const Vec3& pos,
			const Vec3& scale,
			const Vec3& rot,
			const int& Layer,
			const Col4& color,
			const wstring& Textures
		) :
			UI_Animation(
				StagePtr,
				L,
				R,
				U,
				D,
				pos,
				scale,
				rot,
				Layer,
				color,
				Textures
			)
		{}
		virtual ~UI_GameFinish() {};
		virtual void OnCreate() override;
		virtual void OnUpdate() override;
	};



	class UI_TitleLogo : public GameObject {
		float m_time = 0.0f;
		bool m_draw = 0;
	public:
		UI_TitleLogo(const shared_ptr<Stage>& StagePtr) : GameObject(StagePtr) {}
		virtual ~UI_TitleLogo() {};
		virtual void OnCreate() override;

	};

	class UI_TitleLogoA : public UI_Animation {
	public:
		UI_TitleLogoA(const shared_ptr<Stage>& StagePtr,
			const float& L,
			const float& R,
			const float& U,
			const float& D,
			const Vec3& pos,
			const Vec3& scale,
			const Vec3& rot,
			const int& Layer,
			const Col4& color,
			const wstring& Textures
		) :
			UI_Animation(
				StagePtr,
				L,
				R,
				U,
				D,
				pos,
				scale,
				rot,
				Layer,
				color,
				Textures
			)
		{}
		virtual ~UI_TitleLogoA() {};
		virtual void OnCreate() override;
		virtual void OnUpdate() override;

	};
	
	class UI_TitleLogoB : public UI_Animation {
	public:
		UI_TitleLogoB(const shared_ptr<Stage>& StagePtr,
			const float& L,
			const float& R,
			const float& U,
			const float& D,
			const Vec3& pos,
			const Vec3& scale,
			const Vec3& rot,
			const int& Layer,
			const Col4& color,
			const wstring& Textures
		) :
			UI_Animation(
				StagePtr,
				L,
				R,
				U,
				D,
				pos,
				scale,
				rot,
				Layer,
				color,
				Textures
			)
		{}
		virtual ~UI_TitleLogoB() {};
		virtual void OnCreate() override;
		virtual void OnUpdate() override;

	};

	class UI_TitleLogoAB : public UI_Animation {
		int m_draw = 0;
	public:
		UI_TitleLogoAB(const shared_ptr<Stage>& StagePtr,
			const float& L,
			const float& R,
			const float& U,
			const float& D,
			const Vec3& pos,
			const Vec3& scale,
			const Vec3& rot,
			const int& Layer,
			const Col4& color,
			const wstring& Textures
		) :
			UI_Animation(
				StagePtr,
				L,
				R,
				U,
				D,
				pos,
				scale,
				rot,
				Layer,
				color,
				Textures
			)
		{}
		virtual ~UI_TitleLogoAB() {};
		virtual void OnCreate() override;
		virtual void OnUpdate() override;

	};

	class UI_TitleLogoC : public UI_Animation {
	public:
		UI_TitleLogoC(const shared_ptr<Stage>& StagePtr,
			const float& L,
			const float& R,
			const float& U,
			const float& D,
			const Vec3& pos,
			const Vec3& scale,
			const Vec3& rot,
			const int& Layer,
			const Col4& color,
			const wstring& Textures
		) :
			UI_Animation(
				StagePtr,
				L,
				R,
				U,
				D,
				pos,
				scale,
				rot,
				Layer,
				color,
				Textures
			)
		{}
		virtual ~UI_TitleLogoC() {};
		virtual void OnCreate() override;
		virtual void OnUpdate() override;
	};

	class UI_TitleLogoD : public UI_Animation {
		int m_draw = 0;
	public:
		UI_TitleLogoD(const shared_ptr<Stage>& StagePtr,
			const float& L,
			const float& R,
			const float& U,
			const float& D,
			const Vec3& pos,
			const Vec3& scale,
			const Vec3& rot,
			const int& Layer,
			const Col4& color,
			const wstring& Textures
		) :
			UI_Animation(
				StagePtr,
				L,
				R,
				U,
				D,
				pos,
				scale,
				rot,
				Layer,
				color,
				Textures
			)
		{}
		virtual ~UI_TitleLogoD() {};
		virtual void OnCreate() override;
	};

	class UI_TitleAPush : public UI_Animation {
	public :
		UI_TitleAPush(const shared_ptr<Stage>& StagePtr,
			const float& L,
			const float& R,
			const float& U,
			const float& D,
			const Vec3& pos,
			const Vec3& scale,
			const Vec3& rot,
			const int& Layer,
			const Col4& color,
			const wstring& Textures
		) :
			UI_Animation(
				StagePtr,
				L,
				R,
				U,
				D,
				pos,
				scale,
				rot,
				Layer,
				color,
				Textures
			)
		{}
		virtual ~UI_TitleAPush() {};
		virtual void OnCreate() override;
		virtual void OnUpdate() override;

	};



	class UI_MapSelect : public UI_Animation {
	public:
		UI_MapSelect(const shared_ptr<Stage>& StagePtr,
			const float& L,
			const float& R,
			const float& U,
			const float& D,
			const Vec3& pos,
			const Vec3& scale,
			const Vec3& rot,
			const int& Layer,
			const Col4& color,
			const wstring& Textures
		) :
			UI_Animation(
				StagePtr,
				L,
				R,
				U,
				D,
				pos,
				scale,
				rot,
				Layer,
				color,
				Textures
			)
		{}
		virtual ~UI_MapSelect() {};
		virtual void OnCreate() override;
		virtual void OnUpdate() override;
	};

	class UI_MapTextSelect : public UI_Animation {
	public:
		UI_MapTextSelect(const shared_ptr<Stage>& StagePtr,
			const float& L,
			const float& R,
			const float& U,
			const float& D,
			const Vec3& pos,
			const Vec3& scale,
			const Vec3& rot,
			const int& Layer,
			const Col4& color,
			const wstring& Textures
		) :
			UI_Animation(
				StagePtr,
				L,
				R,
				U,
				D,
				pos,
				scale,
				rot,
				Layer,
				color,
				Textures
			)
		{}
		virtual ~UI_MapTextSelect() {};
		virtual void OnCreate() override;
		virtual void OnUpdate() override {};
	};

	class UI_MapPicSelect : public UI_Animation {
		Col4 m_color;
		int m_layer = 0;
		int m_textures = 10;
		wstring m_text[10];
		int m_select = 0;
		bool m_stick = true;
		bool m_scene = true;
	public:
		UI_MapPicSelect(const shared_ptr<Stage>& StagePtr,
			const float& L,
			const float& R,
			const float& U,
			const float& D,
			const Vec3& pos,
			const Vec3& scale,
			const Vec3& rot,
			const int& Layer,
			const Col4& color,
			const wstring& Textures
		) :
			UI_Animation(
				StagePtr,
				L,
				R,
				U,
				D,
				pos,
				scale,
				rot,
				Layer,
				color,
				Textures
			)
		{}
		virtual ~UI_MapPicSelect() {};
		virtual void OnCreate() override;
		virtual void OnUpdate() override;
		void SelectMove();
	};

	class UI_MapBackSelect : public UI_Animation {
		wstring m_textures[10];
	public:
		UI_MapBackSelect(const shared_ptr<Stage>& StagePtr,
			const float& L,
			const float& R,
			const float& U,
			const float& D,
			const Vec3& pos,
			const Vec3& scale,
			const Vec3& rot,
			const int& Layer,
			const Col4& color,
			const wstring& Textures
		) :
			UI_Animation(
				StagePtr,
				L,
				R,
				U,
				D,
				pos,
				scale,
				rot,
				Layer,
				color,
				Textures
			)
		{}
		virtual ~UI_MapBackSelect() {};
		virtual void OnCreate() override;
		virtual void OnUpdate() override;
	};




	class UI_ResultLogo : public UI_Animation {
	public:
		UI_ResultLogo(const shared_ptr<Stage>& StagePtr,
			const float& L,
			const float& R,
			const float& U,
			const float& D,
			const Vec3& pos,
			const Vec3& scale,
			const Vec3& rot,
			const int& Layer,
			const Col4& color,
			const wstring& Textures
		) :
			UI_Animation(
				StagePtr,
				L,
				R,
				U,
				D,
				pos,
				scale,
				rot,
				Layer,
				color,
				Textures
			)
		{}
		virtual ~UI_ResultLogo() {};
		virtual void OnCreate() override;
	};

	class UI_ResultSelect : public UI_Animation {
		Vec3 m_pos = Vec3(-250.0f, -300.0f, 0.0f);
		int m_sceneMove = 0;
		bool m_select = true;
		bool m_scene = true;
	public:
		UI_ResultSelect(const shared_ptr<Stage>& StagePtr,
			const float& L,
			const float& R,
			const float& U,
			const float& D,
			const Vec3& pos,
			const Vec3& scale,
			const Vec3& rot,
			const int& Layer,
			const Col4& color,
			const wstring& Textures
		) :
			UI_Animation(
				StagePtr,
				L,
				R,
				U,
				D,
				pos,
				scale,
				rot,
				Layer,
				color,
				Textures
			)
		{}
		virtual ~UI_ResultSelect() {};
		virtual void OnCreate() override;
		virtual void OnUpdate() override;

	};

	class UI_ResultGageBar : public UI_Animation {
		int m_maxCount;
		int m_count;
	public:
		UI_ResultGageBar(const shared_ptr<Stage>& StagePtr,
			const float& L,
			const float& R,
			const float& U,
			const float& D,
			const Vec3& pos,
			const Vec3& scale,
			const Vec3& rot,
			const int& Layer,
			const Col4& color,
			const wstring& Textures
		) :
			UI_Animation(
				StagePtr,
				L,
				R,
				U,
				D,
				pos,
				scale,
				rot,
				Layer,
				color,
				Textures
			)
		{}
		virtual ~UI_ResultGageBar() {};
		virtual void OnCreate() override;
		virtual void OnUpdate() override;
	};
	
	class UI_Result : public UI_Animation {
	public:
		UI_Result(const shared_ptr<Stage>& StagePtr,
			const float& L,
			const float& R,
			const float& U,
			const float& D,
			const Vec3& pos,
			const Vec3& scale,
			const Vec3& rot,
			const int& Layer,
			const Col4& color,
			const wstring& Textures
		) :
			UI_Animation(
				StagePtr,
				L,
				R,
				U,
				D,
				pos,
				scale,
				rot,
				Layer,
				color,
				Textures
			)
		{}
		virtual ~UI_Result() {};
		virtual void OnCreate() override;
	};



	class UI_ResultCount : public GameObject {
		Vec3 m_StartPos;
		wstring m_TextureKey;
		float m_TotalTime = 0;
		//桁数
		UINT m_NumberOfDigits;
		//バックアップ頂点データ
		vector<VertexPositionTexture> m_BackupVertices;
		int m_Layer;
		float m_allBuilding = 0;
		float m_toppleBuilding = 0;

	public:
		UI_ResultCount(const shared_ptr<Stage>& StagePtr, UINT NumberOfDigits,
			const wstring& TextureKey,
			const Vec3& StartPos,
			const int& Layer);
		virtual ~UI_ResultCount() {}

		virtual void OnCreate() override;
		virtual void OnUpdate()override;
	};

	class UI_ResultMaxCount : public GameObject {
		Vec3 m_StartPos;
		wstring m_TextureKey;
		float m_TotalTime = 0;
		//桁数
		UINT m_NumberOfDigits;
		//バックアップ頂点データ
		vector<VertexPositionTexture> m_BackupVertices;
		int m_Layer;

	public:
		UI_ResultMaxCount(const shared_ptr<Stage>& StagePtr, UINT NumberOfDigits,
			const wstring& TextureKey,
			const Vec3& StartPos,
			const int& Layer);
		virtual ~UI_ResultMaxCount() {}

		virtual void OnCreate() override;
		virtual void OnUpdate()override;
	};



	class UI_Copy : public UI_Animation {
		bool m_scene = true;
	public:
		UI_Copy(const shared_ptr<Stage>& StagePtr,
			const float& L,
			const float& R,
			const float& U,
			const float& D,
			const Vec3& pos,
			const Vec3& scale,
			const Vec3& rot,
			const int& Layer,
			const Col4& color,
			const wstring& Textures
		) :
			UI_Animation(
				StagePtr,
				L,
				R,
				U,
				D,
				pos,
				scale,
				rot,
				Layer,
				color,
				Textures
			)
		{}
		virtual ~UI_Copy() {};
		virtual void OnCreate() override;
		virtual void OnUpdate() override;
	};




	class UI_Time : public GameObject {
		Vec3 m_StartPos;
		wstring m_TextureKey;
		float m_TotalTime = 60;
		//桁数
		UINT m_NumberOfDigits;
		//バックアップ頂点データ
		vector<VertexPositionTexture> m_BackupVertices;
		int m_Layer;
		float m_allBuilding = 200;
		float m_toppleBuilding = 0;

	public:
		UI_Time(const shared_ptr<Stage>& StagePtr, UINT NumberOfDigits,
			const wstring& TextureKey,
			const Vec3& StartPos,
			const float& Time,
			const int& Layer);
		virtual ~UI_Time() {}

		virtual void OnCreate() override;
		virtual void OnUpdate()override;
	};

	class UI_TimeBack : public GameObject {
		Vec3 m_StartPos;
		wstring m_TextureKey;
		//桁数
		UINT m_NumberOfDigits;
		//バックアップ頂点データ
		vector<VertexPositionTexture> m_BackupVertices;
		int m_Layer;
	public:
		UI_TimeBack(const shared_ptr<Stage>& StagePtr, UINT NumberOfDigits,
			const wstring& TextureKey,
			const Vec3& StartPos,
			const int& Layer);
		virtual ~UI_TimeBack() {}

		virtual void OnCreate() override;
	};

}