#include "stdafx.h"
#include "Project.h"

namespace basecross {
	void CopywriterStage::CreateCamera() {
		auto PtrView = CreateView<SingleView>();
		//ビューのカメラの設定
		auto PtrCamera = ObjectFactory::Create<Camera>();
		PtrView->SetCamera(PtrCamera);
		PtrCamera->SetEye(0.0f, 0.0f, -10.0f);
		PtrCamera->SetAt(0.0f, 0.0f, 0.0f);
		//マルチライトの作成
		auto PtrMultiLight = CreateLight<MultiLight>();
		//デフォルトのライティングを指定
		PtrMultiLight->SetDefaultLighting();
	}

	void CopywriterStage::CreateUI() {

		AddGameObject<UI_Copy>(
			-1000.0f,
			1000.0f,
			750.0f,
			-750.0f,
			Vec3(0.0f, 0.0f, 0.0f),
			Vec3(1.0f, 1.0f, 1.0f),
			Vec3(0.0f, 0.0f, 0.0f),
			0,
			Col4(1.0f, 1.0f, 1.0f, 1),
			L"Copywriter.png"
			);
	}

	void CopywriterStage::OnCreate() {
		try {
			//ビューとライトの作成
			CreateCamera();
			CreateUI();
		}
		catch (...) {
			throw;
		}
	}

}