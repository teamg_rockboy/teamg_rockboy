#pragma once
#include "stdafx.h"

namespace basecross {
	class Wall_Corner : public ObjectBase {

	public :
		Wall_Corner(const shared_ptr<Stage>& StagePtr,
			const Vec3& Scale,
			const Vec3& Rotation,
			const Vec3& Position,
			const Vec3& ModelScale,
			const Vec3& ModelPosition
			):
			ObjectBase(StagePtr,
				Scale,
				Rotation,
				Position,
				ModelScale,
				ModelPosition
			)
		{}

		~Wall_Corner() {};

		virtual void OnCreate() override;

	};
}