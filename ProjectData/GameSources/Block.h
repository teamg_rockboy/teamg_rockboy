#pragma once
#include "stdafx.h"

namespace basecross {
	class Block : public GameObject {
		Vec3 m_Scale;
		Vec3 m_Rotation;
		Vec3 m_Position;

	public:
		Block(const shared_ptr<Stage>& StagePtr,
			const Vec3& Scale,
			const Vec3& Rotation,
			const Vec3& Position
		);

		virtual ~Block();
		virtual void OnCreate() override;
	};
}
