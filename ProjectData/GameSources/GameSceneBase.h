#pragma once
#include "stdafx.h"

namespace basecross {
	class GameSceneBase : public Stage {
		shared_ptr<SoundItem> m_bgm;

	public:
		GameSceneBase() :Stage() {};

		virtual void OnDestroy()override;

		void PlayBGM(wstring key);

		void InputHander();

	};

}