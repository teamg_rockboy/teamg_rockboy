#include "stdafx.h"
#include "Project.h"

namespace basecross {
	void GameSceneBase::OnDestroy() {
		auto XAudioPtr = App::GetApp()->GetXAudio2Manager();
		XAudioPtr->Stop(m_bgm);
	}

	void GameSceneBase::PlayBGM(wstring key){
		auto XAudioPtr = App::GetApp()->GetXAudio2Manager();
		m_bgm = XAudioPtr->Start(key, XAUDIO2_LOOP_INFINITE, 0.4f);
	}

	void GameSceneBase::InputHander() {
		auto KeyState = App::GetApp()->GetInputDevice().GetKeyState();
		auto cntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		if (KeyState.m_bPressedKeyTbl['P'] || (cntlVec[0].wPressedButtons & XINPUT_GAMEPAD_START)) {
			App::GetApp()->GetScene<Scene>()->SetStageState(StageState::title);
		}
	}

}