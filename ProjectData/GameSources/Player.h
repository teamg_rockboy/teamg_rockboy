/*!
@file Player.h
@brief プレイヤーなど
*/

#pragma once
#include "stdafx.h"

namespace basecross{

	class Player : public ObjectBase {
	public:
		float m_gravityScale;
		Vec3 GetMoveVector() const;
		Vec3 m_speed = Vec3(0.0f, 0.0f, 0.0f);
		Vec3 m_rot = Vec3(0.0f, 0.0f, 0.0f);
		float m_maxSpeed = 100.0f;
		float m_waterSpeed = 30.0f;
		float m_defaltSpeed = 100.0f;
		float m_accSpeed = 2.0f;

		bool m_moveFlag;
		bool m_playSeFlag;
		shared_ptr<SoundItem> m_se;

	public:
		//構築と破棄
		Player(const shared_ptr<Stage>& StagePtr,
			const Vec3& Scale,
			const Vec3& Rotation,
			const Vec3& Position,
			const Vec3& ModelScale,
			const Vec3& ModelPosition,
			float gravityScale
		):
			ObjectBase(StagePtr,
				Scale,
				Rotation,
				Position,
				ModelScale,
				ModelPosition
			),
			m_gravityScale(gravityScale)
		{}

		virtual ~Player();
		//初期化
		virtual void OnCreate() override;
		virtual void OnUpdate() override;
		virtual void OnCollisionEnter(shared_ptr<GameObject>& Other) override;
		virtual void OnCollisionExit(shared_ptr<GameObject>& Other) override;

		Vec3 GetPos() {
			Vec3 pos = GetComponent<Transform>()->GetPosition();
			return pos;
		}

		void PlaySE(wstring se);

		void StopSE();

		void SoundController();

	};
}
//end basecross

