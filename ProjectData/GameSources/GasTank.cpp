#include "stdafx.h"
#include "Project.h"

namespace basecross {
	void GasTank::OnCreate() {
		SetTrans();
		SetModelName(L"m_GasTank.bmf");
		ReadBmfData();
		SetEffectNum(efk_Explosion);
		SetEffectScale(Vec3(2.5f, 2.5f, 2.5f));

		AddTag(L"GasTank");
		auto ptrColl = AddComponent<CollisionObb>();
		ptrColl->SetMakedSize(Vec3(1.0f, 0.988f, 1.0f));

		ptrColl->SetUpdateActive(true);
		ptrColl->SetAfterCollision(AfterCollision::None);

		auto ptrTrans = GetComponent<Transform>();

		//物理計算ボックス
		PsBoxParam param(ptrTrans->GetWorldMatrix(), 100.0f, true, PsMotionType::MotionTypeFixed);
		auto PsPtr = AddComponent<RigidbodyBox>(param);
		
	}

	//
	void GasTank::OnCollisionEnter(shared_ptr<GameObject>& Other) {
		if ((Other->FindTag(L"Player") || Other->FindTag(L"ExpObj") || Other->FindTag(L"Building"))) {
			auto ptrTrans = GetComponent<Transform>();
			auto pos = ptrTrans->GetPosition();
			auto sca = ptrTrans->GetScale() * 4.0f;

			MyDesObject();
			GetStage()->AddGameObject<ExplosionObject>(pos, sca);

		}
	}
}
//end basecross
