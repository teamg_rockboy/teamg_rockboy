#pragma once
#include "stdafx.h"

namespace basecross {

	class ResultStage : public GameSceneBase {
		void CreateCamera();
		void CreateUI();
		bool m_select = false;
		bool m_scene = true;

		int m_brokenObejcNum;
		int m_maxBrakeObject;
		bool m_gameCrearFlag;
		bool m_countStart = false;

		wstring m_bgmName;
	public:
		ResultStage() :GameSceneBase() {}
		virtual ~ResultStage() {}

		virtual void OnCreate()override;
		virtual void OnUpdate()override;

		int GetBrokenObject() {
			return m_brokenObejcNum;
		}

		int GetMaxBrokenObject() {
			return m_maxBrakeObject;
		}

		bool GetCrearFlag() {
			return m_gameCrearFlag;
		}

		void SetCountStart(bool set) {
			m_countStart = set;
		}

		bool GetCountStart() {
			return m_countStart;
		}

		void ReadGameData();
		void SelectBGM();
	};

}