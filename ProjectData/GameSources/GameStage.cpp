/*!
/*!
@file GameStage.cpp
@brief ゲームステージ実体
*/

#include "stdafx.h"
#include "Project.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	//	ゲームステージクラス実体
	//--------------------------------------------------------------------------------------
	void GameStage::CreateViewLight() {
		auto ptrView = CreateView<SingleView>();
		//ビューのカメラの設定
		auto ptrMyCamera = ObjectFactory::Create<MyCamera>();
		//auto ptrMyCamera = ObjectFactory::Create<Camera>();
		ptrView->SetCamera(ptrMyCamera);
		auto CameraEye = Vec3(0.0f,40.0f,0.0f);
		ptrMyCamera->SetEye(CameraEye);
		ptrMyCamera->SetAt(Vec3(0.0f, 0.0f, 0.0f));
		//マルチライトの作成
		auto ptrMultiLight = CreateLight<MultiLight>();
		//デフォルトのライティングを指定
		ptrMultiLight->SetDefaultLighting();

	}

	void GameStage::CreateEffect() {
		m_efkInterface = ObjectFactory::Create<EfkInterface>();
	}

	void GameStage::CreateGround() {

		//scale
		//rotate
		//position

		float groundRad = 260.0f;

		AddGameObject<Ground>(
			Vec3(groundRad, 0.1f, groundRad),
			Vec3(0.0f, 0.0f, 0.0f),
			Vec3(0.0f, -1.0f, 4.0f));
	}

	void GameStage::CreatePlayer() {

		//scale
		//rotate
		//position
		//gravity
		auto ptrPlayer = AddGameObject<Player>(
			Vec3(5.0f),
			Vec3(0.0f),
			Vec3(0.0f, 5.0f, 0.0f),
			Vec3(2.0f),
			Vec3(0.0f,-0.55f,0.0f),
			2000.0f);
		SetSharedGameObject(L"Player", ptrPlayer);
	}

	void GameStage::CreateUI() {
		AddGameObject<UI_RockBoy>(
			false,
			3
			);

		AddGameObject<UI_GameStage>(
			-300.0f,
			300.0f,
			60.0f,
			-60.0f,
			Vec3(-600.0f, -450.0f, 0.0f),
			Vec3(1.0f, 1.0f, 1.0f),
			Vec3(0.0f, 0.0f, 0.0f),
			10,
			Col4(1.0f, 1.0f, 1.0f, 1),
			L"UI_houkai.png"
			);
		AddGameObject<GageBar>(
			-290.0f,
			290.0f,
			60.0f,
			-60.0f,
			Vec3(-600.0f, -450.0f, 0.0f),
			Vec3(1.0f, 1.0f, 1.0f),
			Vec3(0.0f, 0.0f, 0.0f),
			11,
			Col4(0.5f, 0.5f, 0.5f, 1),
			L"UI_houkaikara.png"
			);
		AddGameObject<UI_GameStage>(
			-300.0f,
			300.0f,
			60.0f,
			-60.0f,
			Vec3(-600.0f, -450.0f, 0.0f),
			Vec3(1.0f, 1.0f, 1.0f),
			Vec3(0.0f, 0.0f, 0.0f),
			12,
			Col4(1.0f, 1.0f, 1.0f, 1),
			L"UI_memori.png"
			);


		AddGameObject<UI_GameStage>(
			-1000.0f,
			1000.0f,
			550.0f,
			-550.0f,
			Vec3(0.0f, 0.0f, 0.0f),
			Vec3(1.0f, 1.0f, 1.0f),
			Vec3(0.0f, 0.0f, 0.0f),
			-10,
			Col4(1.0f, 1.0f, 1.0f, 1),
			L"sky_texture.jpg"
			);

		AddGameObject<UI_Time>(
			3,
			L"Text_Number.png",
			Vec3(0.0f, 480.0f, 0.0f),
			m_stageTime,
			51
			);

		AddGameObject<UI_ResultLogo>(
			-160.0f,
			160.0f,
			160.0f,
			-160.0f,
			Vec3(0.0f, 470.0f, 0.0f),
			Vec3(1.0f, 1.0f, 1.0f),
			Vec3(0.0f, 0.0f, 0.0f),
			12,
			Col4(1.0f, 1.0f, 1.0f, 0.8f),
			L"Text_Timer.png"
			);

	}

	void GameStage::OnCreate() {
		try {
			//ビューとライトの作成
			CreateEffect();

			CreateEffectData();

			ReadGameData();
			CreateViewLight();
			CreateGround();
			CreatePlayer();
			
			m_MaxDestroyBuilTime = 1.5f;
			m_DestroyBuilTime = m_MaxDestroyBuilTime;

			AddGameObject<MapCreator>();
			CreateUI();

			PlayBGM(L"game_maoudamashii_5_town22.wav");

			//m_gameStart = true;
			//AddGameObject<UI_GameStage>(
			//	-1000.0f,
			//	1000.0f,
			//	550.0f,
			//	-550.0f,
			//	Vec3(0.0f, 0.0f, 0.0f),
			//	Vec3(1.0f, 1.0f, 1.0f),
			//	Vec3(0.0f, 0.0f, 0.0f),
			//	-10,
			//	Col4(1.0f, 1.0f, 1.0f, 1),
			//	L"sky_texture.jpg"
			//	);

		}
		catch (...) {
			throw;
		}
	}

	void GameStage::OnUpdate() {
		m_efkInterface->OnUpdate();

		DestroyBuilTime();
		if (m_gameFinish) {
			AddGameObject<UI_GameFinish>(
				-800.0f,
				800.0f,
				200.0f,
				-200.0f,
				Vec3(1700.0f, 0.0f, 0.0f),
				Vec3(1.01f, 1.01f, 1.01f),
				Vec3(0.0f, 0.0f, 0.0f),
				10,
				Col4(1.0f, 1.0f, 1.0f, 1),
				L"Tezt_End.png"
				);
			m_gameFinish = false;
		}
		
		InputHander();
	}

	void GameStage::OnDraw() {
		auto &camera = GetView()->GetTargetCamera();
		m_efkInterface->SetViewProj(camera->GetViewMatrix(),camera->GetProjMatrix());
		m_efkInterface->OnDraw();
	}

	void GameStage::AddMaxBreakObject(int num) {
		m_maxBreakObject += num;
	}

	void GameStage::AddBrokenObject(int num) {
		m_brokenObject += num;
	}

	void GameStage::AddFallBuilObject(shared_ptr<Building> builObject) {
		m_FallBuilObject.push_back(builObject);
		m_DestroyBuilTime = m_MaxDestroyBuilTime;
		m_IsCountTime = true;
	}
	
	void GameStage::CreateEffectData() {
		wstring mediaDir;
		App::GetApp()->GetDataDirectory(mediaDir);
		mediaDir = mediaDir + L"Effect/";

		wstring efkDir = mediaDir + L"explosion.efk";
		auto efkEffect = ObjectFactory::Create<EfkEffect>(m_efkInterface, efkDir);
		m_efkEffect.push_back(efkEffect);

		efkDir = mediaDir + L"hakai.efk";
		efkEffect = ObjectFactory::Create<EfkEffect>(m_efkInterface, efkDir);
		m_efkEffect.push_back(efkEffect);

		efkDir = mediaDir + L"smoke.efk";
		efkEffect = ObjectFactory::Create<EfkEffect>(m_efkInterface, efkDir);
		m_efkEffect.push_back(efkEffect);
	}

	void GameStage::AddEfkPlay(shared_ptr<EfkPlay> efkPlay) {
		m_efkPlay.push_back(efkPlay);
	}

	void GameStage::DestroyBuilTime() {
		if (m_IsCountTime) {
			auto delta = App::GetApp()->GetElapsedTime();
			m_DestroyBuilTime -= delta;

			if (m_DestroyBuilTime <= 0.0f) {
				DestroyBuil();
				m_DestroyBuilTime = m_MaxDestroyBuilTime;
				m_IsCountTime = false;
			}
		}
	}

	void GameStage::DestroyBuil() {
		for (int i = 0; i < m_FallBuilObject.size(); i++) {
			m_FallBuilObject[i]->MyDesObject();
		}
		m_FallBuilObject.clear();
	}

	void GameStage::ReadGameData() {
		wstring DataDir;
		auto stageKey = App::GetApp()->GetScene<Scene>()->GetStageKey();
		wstring stageNode = L"MapData/Time/" + stageKey;

		App::GetApp()->GetDataDirectory(DataDir);
		m_XmlDocReader.reset(new XmlDocReader(DataDir + L"XML/" + L"MapData.xml"));

		auto mapTimeNode = m_XmlDocReader->GetSelectSingleNode(stageNode.c_str());
		wstring SetMapTimeStr = XmlDocReader::GetText(mapTimeNode);

		m_stageTime = (int)stoi(SetMapTimeStr);
	}

	void GameStage::WriteGameData() {
		wstring ss, brokenObject, maxBrakeObject, clearFlag;
		App::GetApp()->GetDataDirectory(ss);
		bool c = false;

		auto key = new XmlDocWriter(ss + L"/XML/" + L"GameData.xml");

		wstring crear;

		float m_raito = ((float)m_brokenObject / (float)m_maxBreakObject) * 100.0f;

		if (m_raito >= 70.0f) {
			crear = L"true";
		}
		else {
			crear = L"false";
		}

		vector<wstring> SaveGameDataStr = {
			Util::FloatToWStr((float)m_brokenObject, 1, Util::Fixed),
			Util::FloatToWStr((float)m_maxBreakObject, 1, Util::Fixed),
			crear
		};

		vector<IXMLDOMNodePtr> GameDataNode = {
			key->GetSelectSingleNode(L"GameData/BrokenObject"),
			key->GetSelectSingleNode(L"GameData/MaxBrakeObject"),
			key->GetSelectSingleNode(L"GameData/ClearFlag")
		};


		for (int i = 0; i < GameDataNode.size(); i++) {
			key->SetText(GameDataNode[i], SaveGameDataStr[i].c_str());
		}

		key->Save(ss + L"/XML/" + L"GameData.xml");
	}
}
//end basecross
