#pragma once
#include "stdafx.h"

namespace basecross {
	class CopywriterStage : public Stage{
		void CreateCamera();
		void CreateUI();

	public :
		CopywriterStage() : Stage(){};
		~CopywriterStage() {};

		virtual void OnCreate()override;
	};
}